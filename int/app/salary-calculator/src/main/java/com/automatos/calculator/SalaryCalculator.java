
package com.automatos.calculator;

/**
 * Salary Calculator
 *
 */
public class SalaryCalculator {

    private Integer baseSalary;

    public SalaryCalculator(Integer monthlySalary) {
        this.baseSalary = monthlySalary;
    }

    public Integer getBaseSalary() {
        return this.baseSalary;
    }


}
