package com.automatos.annotation_examples;

import org.junit.*;

public class BasicAnnotationTest {

    @BeforeClass
    public static void runOnceBeforeClass() {
        System.out.println("@BeforeClass - Open the gate");
    }

    @AfterClass
    public static void runOnceAfterClass() {
        System.out.println("@AfterClass - Shut the gate");
    }

    @Before
    public void beforeEachTest() {
        System.out.println("\n    @Before - Turn on the room light");
    }

    @After
    public void afterEachTest() {
        System.out.println("    @After - Turn off the room light\n    ");
    }

    @Test
    public void kitchenIsClean() {
        System.out.println("    @Test - Kitchen is clean");
    }

    @Test
    public void livingRoomIsClean() {
        System.out.println("    @Test - Living room is clean");
    }

}