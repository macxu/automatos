package com.automatos.calculator;

public class SalaryCalculatorTest_01 {

    public static void main(String []args) {

        Integer salary = 10000;

        SalaryCalculator calculator = new SalaryCalculator(salary);

        Integer actualSalary = calculator.getBaseSalary();

        if (salary != actualSalary) {
            System.out.println("Salary not match!");
        }
    }
}