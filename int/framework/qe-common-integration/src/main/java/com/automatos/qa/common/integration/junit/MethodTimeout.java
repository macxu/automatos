package com.automatos.qa.common.integration.junit;

import com.automatos.qa.common.integration.env.TestEnv;
import com.automatos.qa.common.integration.fojos.TestResource;
import com.automatos.qa.commoncore.exception.QaRuntimeException;
import org.junit.rules.Timeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 */
public class MethodTimeout extends Timeout {
    
    private static final Logger log = LoggerFactory.getLogger(TestResource.class);
    private static final String ENV_PROPERTIES_TIMEOUT = "timeout.test.method.minutes";

    public MethodTimeout() {
        super(getTimeoutFromEnvProperties());
    }

    public MethodTimeout(int milliseconds) {
        super(milliseconds);

    }
    
    public static int getTimeoutFromEnvProperties() {
        int timeoutValue;

        Properties properties = TestEnv.getInstance().getEnvPropertyBundle();
        if (properties != null) {
            String value = properties.getProperty(ENV_PROPERTIES_TIMEOUT);
            if (value != null) {                
                timeoutValue = Integer.parseInt(value);
            } else {
                throw new QaRuntimeException("environment property: '" + ENV_PROPERTIES_TIMEOUT + "' not found");
            }
        } else {
            throw new QaRuntimeException("environment property: '" + ENV_PROPERTIES_TIMEOUT + "' not found");
        }
        
        log.info("Test method timeout set to {} minutes", timeoutValue);
        return (int)TimeUnit.MINUTES.toMillis(timeoutValue);   //Timeout value expected in milliseconds
    }

}
