package com.automatos.qa.common.integration.enums;

/**
 * Enum representing the publishers supported by marin
 */
public enum PublisherEnum {
    GOOGLE(4, "Google", "google", 0.01f, 1000.0f),
    YAHOO(5, "Yahoo", null, null),
    BING(6, "Msn", 0.05f, 1000.0f),
    FACEBOOK(7, "Facebook", null, null),
    BAIDU(8, "Baidu", 0.01f, 999.99f),
    YAHOO_JAPAN(9, "Yahoo_Japan", 1.0f, 9999.0f);

    private int id;
    private String publisher;
    private int publisherKey;
    private int productTargetKey;
    private int sitelinkKey;
    private String envPropertyName;
    private final Float publisherMaxCpc;
    private final Float publisherMinCpc;

    PublisherEnum(int id, String publisher, Float minCpc, Float maxCpc) {
        this.id = id;
        this.publisher = publisher;

        /*
         Here is sub form url params, these maps are matching the param for each publisher attributes.
         The key is the publisher url builder key id number now is publisher id * 10000 + url type to avoid key conflict;
         The value string is the the param values. E.g. buildKeywordUrls need boolean string "true" or "false" as attribute value.
         The key and value are following the url build internal key and value rules.
         The key generator rule is following frontend code to separate the publisher and url type
         /front_end/modules/url_builder/url_builder.htm.php line 321
         E.g. urlBuilderType <40002, "SITELINK"> which means 40002 as google site link and publisher type is SITE LINK.
        */
        this.publisherKey = id * 10000;
        this.productTargetKey = publisherKey + 1;
        this.sitelinkKey = publisherKey + 2;

        /*
         * These two columns are taken from
         * publisher_definitions.publisher_max_cpc and
         * publisher_definitions.publisher_min_cpc
         */
        this.publisherMinCpc = minCpc;
        this.publisherMaxCpc = maxCpc;
    }

    /**
     * Alternate constructor to specify the envPropertyName that is used in
     * the env.properties files
     *
     * @param id
     * @param publisher
     * @param envPropertyName
     * @param minCpc value of the publisher_definitions.publisher_min_cpc
     * @param maxCpc value of the publisher_definitions.publisher_max_cpc
     */
    PublisherEnum(int id, String publisher, String envPropertyName, Float minCpc, Float maxCpc) {
        this(id, publisher, minCpc, maxCpc);
        this.envPropertyName = envPropertyName;
    }

    /**
     * Get the publisher with the given id
     * @param id - the id of the publisher
     * @return the matching publisher, or null if no publisher has the given id
     */
    public static PublisherEnum fromId(int id) {
        for (PublisherEnum publisher : PublisherEnum.values()) {
            if (publisher.id == id) {
                return publisher;
            }
        }
        return null;
    }

    /**
     * Get the publisher with the given id
     * @param id - the id of the publisher
     * @return the matching publisher, or null if no publisher has the given id
     */
    public static PublisherEnum fromId(long id) {
        return fromId((int)id);
    }

    /**
     * Get the publisher with the given id
     * @param publisherKey - the publisher key of the publisher
     * @return the matching publisher, or null if no publisher has the given publisherKey
     */
    public static PublisherEnum fromPublisherKey(int publisherKey) {
        for (PublisherEnum publisher : PublisherEnum.values()) {
            if (publisher.publisherKey == publisherKey) {
                return publisher;
            }
        }
        return null;
    }

    /**
     * Returns the publisher with the given publisher name or null if
     * none exists.
     *
     * @param enumName
     *            the publisher name
     * @return the matching publisher or {@code null} if no none
     *         exists.
     */
    public static PublisherEnum fromName(String enumName) {
        for (PublisherEnum p : values()) {
            if (p.name().equals(enumName)) {
                return p;
            }
        }

        return null;
    }

    /**
     * Return the publisher with the given publisher value or null if none exists.
     * Publisher value is equals ignore case
     *
     * @param publisher publisher value
     * @return the matching publisher or {@code null} if no none exists.
     */
    public static PublisherEnum fromPublisher(String publisher) {
        for (PublisherEnum p : values()) {
            if (p.getPublisher().equalsIgnoreCase(publisher)) {
                return p;
            }
        }
        return null;
    }

    /**
     * @return the publisher
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * @return publisher id as string
     */
    public String pubIdToString() {
        return Integer.toString(id);
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    public int getPublisherKey() {
        return publisherKey;
    }

    public int getSitelinkKey() {
        return sitelinkKey;
    }

    public int getProductTargetKey() {
        return productTargetKey;
    }

    public String getEnvPropertyName(){
        return envPropertyName;
    }

    public Float getPublisherMinCpc() {
        return publisherMinCpc;
    }

    public Float getPublisherMaxCpc() {
        return publisherMaxCpc;
    }
}
