package com.automatos.qa.common.integration.rest;

import com.automatos.qa.commoncore.url.QaURL;
import com.automatos.qa.commoncore.exception.QaRuntimeException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class for performing simple rest calls over http
 *
 */
public class QaHttpClient {
    private static final Logger classLog = LoggerFactory.getLogger(QaHttpClient.class);

    private final CloseableHttpClient httpClient = HttpClientBuilder.create().build();

    public String getString(String urlString) {

        QaURL url = new QaURL(urlString);
        HttpGet httpGet = new HttpGet(url.getUri());

        HttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);

        } catch (IOException e) {
            throw new QaRuntimeException(e);
        }

        StringBuilder result = new StringBuilder();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        } finally {
            EntityUtils.consumeQuietly(response.getEntity());
        }

        return result.toString();
    }

    public JSONObject getJson(String urlString) {
        String response = getString(urlString);

        try {
            return new JSONObject(response);
        } catch (JSONException e) {
            throw new QaRuntimeException(e);
        }
    }
}
