package com.automatos.qa.common.integration.testdefinition;

import com.automatos.qa.commoncore.junit.log.TestLogger;

import java.util.List;

/**
 * basic Test definition
 *
 */
public class BaseTestDefinition {

    // there are the mandatory fields which are for test design
    private String description;
    private String priority;
    private String jira;
    private List<String> checkpoints;
    private Boolean positiveCase;
    private String service;
    private String feature;
    private String verb;
    private Boolean automated;
    private String author;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isAutomated() {
        if (automated == null) {
            return true;
        }

        return automated;
    }

    public void setAutomated(Boolean automated) {
        this.automated = automated;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public List<String> getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(List<String> checkpoints) {
        this.checkpoints = checkpoints;
    }

    public Boolean isPositiveCase() {
        return positiveCase;
    }

    public void setPositiveCase(Boolean positiveCase) {
        this.positiveCase = positiveCase;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getVerb() {
        return verb;
    }

    public void setVerb(String verb) {
        this.verb = verb;
    }

    public String getJira() {
        return jira;
    }

    public void setJira(String jira) {
        this.jira = jira;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void describe(TestLogger logger) {
        logger.info(description);

        if (author != null) {
            logger.info("\tAuthor: " + author);
        }

        if (priority != null) {
            logger.info("\tPriority: " + priority);
        } else {
            logger.info("\tPriority: NOT SPECIFIED");
        }

        if (checkpoints != null) {
            logger.info("\tCheckpoints: ");
            for(String checkpoint : checkpoints) {
                logger.info("\t - " + checkpoint);
            }
        } else {
//            logger.info("\tCheckpoints: NOT SPECIFIED");
        }

        if (service != null) {
            logger.info("\tService: " + service);
        } else {
            logger.info("\tService: NOT SPECIFIED");
        }

        if (feature != null) {
            logger.info("\tFeature: " + feature);
        } else {
            logger.info("\tFeature: NOT SPECIFIED");
        }

        if (verb != null) {
            logger.info("\tVerb: " + verb);
        } else {
//            logger.info("\tVerb: NOT SPECIFIED");
        }

    }

    @Override
    public String toString() {
        return description;
    }
}
