package com.automatos.qa.common.integration.fojos;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 3/14/17.
 */
public interface FojoInterface {

    Map<String, SpringResource> registeredServices = new HashMap<>();

    default <T> T getFojo(String fojoName, Class<T> returnType) {
        SpringResource marinResource = registeredServices.get(fojoName);
        verifyInitialized(marinResource);

        return marinResource.ctx().getBean(fojoName, returnType);
    }

    default void verifyInitialized(SpringResource resource) {

        if (!resource.isInitialized()) {
            throw new QaRuntimeException(resource.getName() + " is not initialized.  Check the upstream logs.");
        }
    }

    default void registerService(String name, String displayName, String configFileLocation) {
        SpringResource springResource = new SpringResource(displayName).setConfigs(configFileLocation);

        if (registeredServices.containsKey(name)) {
            throw new QaRuntimeException(name + " is already registered.  Please use a unique name when adding to TestResource");
        }
        registeredServices.put(name, springResource);
    }

    default void initializeResouces() {
        for (SpringResource springResource : registeredServices.values()) {
            springResource.before();
        }
    }

    default void closeResources() {
        for (SpringResource springResource : registeredServices.values()) {
            springResource.after();
        }
    }
}