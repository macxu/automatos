package com.automatos.qa.common.integration.entities;

/**
 * Base integration vo object with common use fields
 */
public class BasePvo {
    private Long id;
    private Long extId;
    private Long parentId;
    private String name;
    private String parentName;
    private String status;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return this.parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentName() {
        return this.parentName;
    }

    public void setParentName(String name) {
        this.parentName = name;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getExtId() {
        return extId;
    }

    public void setExtId(Long extId) {
        this.extId = extId;
    }

    public void normalize() {
        if (id == null) {
            id = 0L;
        }
        if (parentId == null) {
            parentId = 0L;
        }
        if (name != null && name.trim().isEmpty()) {
            name = null;
        }
        if (parentName != null && parentName.trim().isEmpty()) {
            parentName = null;
        }
        if (status != null && status.trim().isEmpty()) {
            status = null;
        }
        if (extId == null) {
            extId = 0L;
        }


    }

    @Override
    public String toString() {
        return "BasePvo [id=" + id + ", parentId=" + parentId + ", name=" + name + ", parentName=" + parentName + ", status=" + status + ", extId=" + extId + "]";
    }
}
