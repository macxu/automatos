package com.automatos.qa.common.integration.env;

import com.automatos.qa.common.integration.enums.PublisherEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A representation of the Marin shared environment properties.
 *
 * Created by mxu on 5/20/15.
 */
public class TestSharedEnvProperties {

    private static final Logger log = LoggerFactory.getLogger(TestSharedEnvProperties.class);

    private TestAccounts testAccounts;

    public TestAccounts testAccounts() {
        if (testAccounts == null) {
            testAccounts = new TestAccounts();
        }

        return testAccounts;
    }

    /**
     * Get the MarinPca object based on the given conditions.
     *
     * @param publisherEnum the publisher of the pca
     * @param category the category of use of the pca, like "bulk", "voservice" etc.
     *
     * @return MarinPca
     */
    public TestAccount getPca(PublisherEnum publisherEnum, String category) {
        return testAccounts().getPca(publisherEnum, category);
    }

}
