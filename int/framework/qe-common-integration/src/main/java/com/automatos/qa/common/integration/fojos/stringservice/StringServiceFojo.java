package com.automatos.qa.common.integration.fojos.stringservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * See {@link StringServiceRestClient} for javadoc
 */
public class StringServiceFojo {
    private static final Logger classLog = LoggerFactory.getLogger(StringServiceFojo.class);

    @Autowired
    StringServiceRestClient restClient;

    public StringServiceFojo() {
        restClient = new StringServiceRestClient();
    }

    public String reverseAndMerge(String originalString) {

        return restClient.reverseAndMerge(originalString);
    }

}