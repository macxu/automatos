package com.automatos.qa.common.integration.fojos.stringservice;

import com.automatos.qa.common.integration.rest.SpringRestClient;
import com.automatos.qa.commoncore.url.QaURL;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  Rest Client for talking to the String Service
 */
public class StringServiceRestClient {
    private static final Logger classLog = LoggerFactory.getLogger(StringServiceRestClient.class);

    private SpringRestClient springRestClient;
    private String baseURL;

    public StringServiceRestClient() {
        springRestClient = new SpringRestClient();
    }
    
    public void setBaseUrl(String baseURL) {
        String baseURLTemp = baseURL;

        if (!StringUtils.isEmpty(baseURL) && !baseURL.endsWith("/")) {
            baseURLTemp += "/";
        }
        this.baseURL = baseURLTemp;
    }
    
    public String getBaseUrl() {
        return baseURL;
    }

    public String reverseAndMerge(String originalString) {
        QaURL qaURL = new QaURL(baseURL + "api/string/reverse_and_merge/" + originalString);

        return springRestClient.get(qaURL, String.class);
    }

}