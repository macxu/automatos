package com.automatos.qa.common.integration.rest;

import org.apache.commons.httpclient.HttpStatus;

/**
 * The <code>Status</code> enumeration contains values that can be used within a request/response
 */
public class QaHttpStatus extends HttpStatus {
    public QaHttpStatus() {
        super();
    }

    //This is for Http status code grouping by code /100, pick the first number for response group.
    public enum StatusClass { INFORMATIONAL, SUCCESSFUL, REDIRECTION, CLIENT_ERROR, SERVER_ERROR, UNKNOWN }

    public static StatusClass assignFamily(int code) {
        StatusClass statusClass;
        switch( code / 100 ) {
            case 1:
                statusClass = StatusClass.INFORMATIONAL;
                break;
            case 2:
                statusClass = StatusClass.SUCCESSFUL;
                break;
            case 3:
                statusClass = StatusClass.REDIRECTION;
                break;
            case 4:
                statusClass = StatusClass.CLIENT_ERROR;
                break;
            case 5:
                statusClass = StatusClass.SERVER_ERROR;
                break;
            default:
                statusClass = StatusClass.UNKNOWN;
                break;
        }
        return statusClass;
    }
    
    public StatusClass getStatusClass(int code) {
        return assignFamily(code);
    }
}
