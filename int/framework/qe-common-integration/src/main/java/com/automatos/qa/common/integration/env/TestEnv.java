package com.automatos.qa.common.integration.env;

import com.automatos.qa.common.integration.fojos.TestResource;
import com.automatos.qa.commoncore.string.QaStringUtils;
import com.automatos.qa.commoncore.exception.QaRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

/**
 * A representation of the -Dtest.env argument which specifies the hosts we are using.
 * This class will look for "test.env" first a System argument, then in the environment.
 * If neither is found, it will default to localhost.
 */
public class TestEnv {

    public static final String OVERRIDE_KEY_PREFIX = "framework.";

    private static final Logger LOG = LoggerFactory.getLogger(TestEnv.class);

    private static final String DEFAULTHOST = "localhost";
    private static final String PROPERTY_FILE_PATH = "/conf/env/%s/env.properties";

    // MARIN_ENV_KEY can be updated during the VM lifetime to supporting single test case against two environments.
    private String activeMarinEnvKey = "test.env";
    public static final String TEST_ENV_KEY = "test.env";
    private static final String VM_NAME_TOKEN = "[VMNAME]";

    private static final String TEST_ENV_DOMAIN_KEY = "test.env.domain";
    private static final String VM_DOMAIN_TOKEN = "[VMDOMAIN]";
    private static final String DEFAULT_DOMAIN = "automatos.com";

    // clusters
    private static final String TEST_CLUSTER_KEY = "test.cluster";
    private static final String TEST_CLUSTER_PROPERTY_FILE_PATH = "/conf/env/clusters/cluster.%s.properties";

    private boolean needToLogEnvs = true;

    private String testEnv = null;
    private String testEnvDomain = null;
    private String testCluster = null;

    private Properties testEnvProperties;

    private static TestEnv instance;

    private TestEnv() {

        // Checking test.env environment
        if (testEnv == null) {
            testEnv = getEnvironmentByKey(activeMarinEnvKey, DEFAULTHOST);
        }

        // Checking test.env.domain environment
        if (testEnvDomain == null) {
            testEnvDomain = getEnvironmentByKey(TEST_ENV_DOMAIN_KEY, DEFAULT_DOMAIN);
        }

        // Checking test.cluster environment
        if (testCluster == null) {
            testCluster = getEnvironmentByKey(TEST_CLUSTER_KEY, null);
        }
    }

    public static TestEnv getInstance() {
        if (instance == null) {
            instance = new TestEnv();
        }
        return instance;
    }

    public void setActiveEnvKey(String marinEnvKey) {

        if (!marinEnvKey.equals(activeMarinEnvKey)) {
            activeMarinEnvKey = marinEnvKey;
            needToLogEnvs = false;
        }
    }

    private String getEnvironmentByKey(String environmentVariableKey, String defaultValue) {

        // check upper case
        String environmentValue =System.getProperty(environmentVariableKey.toUpperCase());

        if (environmentValue == null) {

            // check lower case
            environmentValue = System.getProperty(environmentVariableKey);

        }

        // check env
        if (environmentValue == null) {

            // check env upper case
            environmentValue = System.getenv(environmentVariableKey.toUpperCase());

            if (environmentValue == null) {

                // check env lower
                environmentValue = System.getenv(environmentVariableKey);
            }
        }

        // check env
        if (environmentValue == null) {

            if (needToLogEnvs) {
                LOG.info("Environment {} is not specified, defaulted to '{}'.", environmentVariableKey, defaultValue);
                needToLogEnvs = false;
            }
            environmentValue = defaultValue;
        }
        else {
            if (needToLogEnvs) {
                LOG.info("{} is {}", environmentVariableKey, environmentValue);
                needToLogEnvs = false;
            }
        }

        return environmentValue;
    }

    @Override
    public String toString() {
        return testEnv;
    }

    public String getCluster() {
        return testCluster;
    }

    /**
     * See if the environment specified is of VM or not
     *
     * @return boolean, true if the environment is of VM, otherwise false
     */
    public boolean isVM() {

        return false;
    }

    /**
     * Gets the Properties bundle for the resolved marin environment.
     * It loads the framework level property template first,
     * And then load the user defined property file if it exists.
     *
     * @return Properties
     */
    public Properties getEnvPropertyBundle() {

        if (testEnvProperties == null) {
            testEnvProperties = new Properties();
            loadProperties(testEnvProperties);
        }

        return testEnvProperties;
    }

    /**
     * Gets the Properties bundle for the resolved marin environment.
     * For VM, it loads from a template and replaces the tokenized host names.
     *
     * @return Properties
     */
    public Properties loadProperties(Properties properties) {

        String propertyFile = String.format(PROPERTY_FILE_PATH, testEnv.toLowerCase());
        URL propertyFileResource = TestResource.class.getResource(propertyFile);

        boolean isVmPropertyTemplateUsed = false;
        if (propertyFileResource == null) {
            if (isVM()) {
                // a VM environment is specified but the test project doesn't provide a env.properties, we load the centralized template
                propertyFile = String.format(PROPERTY_FILE_PATH, "vm-template");
                propertyFileResource = TestResource.class.getResource(propertyFile);

                isVmPropertyTemplateUsed = true;
            }
        }

        if (propertyFileResource == null) {
            // if still null, something is wrong
            throw new QaRuntimeException("Unable to load env property file from {} ",propertyFile);
        }
        LOG.info("Full path to loaded centralized property file: {}", propertyFileResource.getPath());

        try {
            properties.load(propertyFileResource.openStream());
        } catch (Exception e) {
            throw new QaRuntimeException("Unable to load env property file: " + propertyFile, e);
        }

        // for VM properties loaded from centralized template, tokenized host names and domain need to be replaced
        if (isVM() && isVmPropertyTemplateUsed) {
            for (Map.Entry<Object, Object> property : properties.entrySet()) {

                String propertyValue = property.getValue().toString();

                if (propertyValue.contains(VM_NAME_TOKEN)) {
                    propertyValue = propertyValue.replace(VM_NAME_TOKEN, testEnv);
                    property.setValue(propertyValue);
                }

                if (propertyValue.contains(VM_DOMAIN_TOKEN)) {
                    propertyValue = propertyValue.replace(VM_DOMAIN_TOKEN, testEnvDomain);
                    property.setValue(propertyValue);
                }
            }
        }

        // load Hadoop properties if required
        if (testCluster != null) {
            propertyFile = String.format(TEST_CLUSTER_PROPERTY_FILE_PATH, testCluster.toLowerCase());
            propertyFileResource = TestResource.class.getResource(propertyFile);

            if (propertyFileResource == null) {
                throw new QaRuntimeException("Unable to find cluster property file from {} ",propertyFile);
            }

            try {
                properties.load(propertyFileResource.openStream());
            } catch (Exception e) {
                throw new QaRuntimeException("Unable to load cluster property file: " + propertyFileResource.getPath(), e);
            }
        }

        return properties;
    }


    public String getEnvResourcePath() {

        return String.format(PROPERTY_FILE_PATH, testEnv);
    }

    public String getFrameworkResourcePath(String scope, String name) {
        String nameTemp = name;
        String path = null;

        String suffix = ".properties";

        if (name.endsWith(suffix)) {
            nameTemp = name.substring(0, name.lastIndexOf(suffix));
        }

        /* any set of framework properties can be overridden from the command line with
            -Dframework.scope=alternate
         */

        if (System.getProperties().containsKey(OVERRIDE_KEY_PREFIX + scope)) {

            nameTemp = System.getProperty(OVERRIDE_KEY_PREFIX + scope);
        } else {

            // any set of properties can also be overriden on a per user basis
            String userName = System.getProperty("user.name");

            path = String.format("/conf/framework/overrides/%s/%s/%s.properties", userName, scope, nameTemp);

            InputStream inputStream = TestResource.class.getResourceAsStream(path);

            if (inputStream == null) {
                path = null;
            }
        }

        // if we didn't have a user override, resolve normally or using the override name if it was set
        if (path == null) {

            path = String.format("/conf/framework/%s/%s.properties", scope, nameTemp);
        }

        LOG.info("Framework resource path: {}", path);

        return path;
    }

    public URL getServiceUrl(String propertyKey) {
        String url = getEnvPropertyBundle().getProperty(propertyKey);

        if (QaStringUtils.isEmpty(url)) {
            throw new QaRuntimeException("No url found for {}", propertyKey);
        }

        // Prefix http:// if it doesn't exist
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }

        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            throw new QaRuntimeException("Url for {} isn't properly formatted", propertyKey, e);
        }
    }
}
