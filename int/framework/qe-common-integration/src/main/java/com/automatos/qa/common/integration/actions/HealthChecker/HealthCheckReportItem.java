package com.automatos.qa.common.integration.actions.HealthChecker;

/**
 *
 */
public class HealthCheckReportItem {

    private String service;
    private Boolean isHealthy;
    private String message;
    private String branch;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Boolean getHealthy() {
        return isHealthy;
    }

    public void setHealthy(Boolean healthy) {
        isHealthy = healthy;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}
