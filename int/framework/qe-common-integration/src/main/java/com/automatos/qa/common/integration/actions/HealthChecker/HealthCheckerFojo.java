package com.automatos.qa.common.integration.actions.HealthChecker;

import com.automatos.qa.common.integration.fojos.TestResource;
import com.automatos.qa.common.integration.rest.SpringRestClient;
import com.automatos.qa.commoncore.string.QaStringUtils;
import com.automatos.qa.commoncore.exception.QaRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class containing all the actions for Health Checker.
 *
 */
public class HealthCheckerFojo {

    private static final Logger logger = LoggerFactory.getLogger(HealthCheckerFojo.class);

    private static final String serviceHealthCheckerUrl = "http://portal.marinsw.net:1701";

    // example value: oozie.status.url=http://labs-kylo-oozie-lv-101.labs.marinsw.net:11000/oozie
    private static String oozieJobHealthCheckUrl;

    private static final SpringRestClient restClient = new SpringRestClient();

    private TestResource resources;
    private String env;

    private Boolean isHealthCheckServiceUp = false;
    private Boolean isOozieJobHealthCheckServiceUp = false;

    private Map<String, List<String>> serviceHostMap;
    private Map<ServiceEnum, Map<String, Object>> oozieJobsMap = new EnumMap<>(ServiceEnum.class);

    public HealthCheckerFojo(TestResource marinResource) {
        this.resources = marinResource;
        this.env = resources.getEnv().toString();

        try {
            restClient.getString(serviceHealthCheckerUrl);
            isHealthCheckServiceUp = true;

            this.serviceHostMap = loadServiceStatus();
        } catch (Exception e) {
            logger.info("Service Health Check API is NOT reachable", e.getMessage());
        }

        oozieJobHealthCheckUrl = resources.getEnv().getEnvPropertyBundle().getProperty("oozie.status.url");
        if (oozieJobHealthCheckUrl == null) {
            logger.info("'oozie.status.url' not found in property file, UNABLE to do health check for Oozie jobs");
            return;
        }

        try {
            restClient.getString(oozieJobHealthCheckUrl);
            isOozieJobHealthCheckServiceUp = true;

            this.oozieJobsMap = loadOozieJobStatus();
        } catch (Exception e) {
            logger.info("Oozie Job Health Check API is NOT reachable", e.getMessage());
        }
    }

    public void check(List<ServiceEnum> servicesToCheckHealth) {

        List<ServiceEnum> oozieJobsToCheck = new ArrayList<>();
        List<ServiceEnum> microServicesToCheck = new ArrayList<>();

//        // group micro-services and oozie jobs, they will be checked in different ways.
//        for (ServiceEnum serviceEnum : servicesToCheckHealth) {
//            if (serviceEnum.isOozieJob()) {
//                oozieJobsToCheck.add(serviceEnum);
//            } else {
//                microServicesToCheck.add(serviceEnum);
//            }
//        }

        String errorMessage = "";

        // micro-services health check.
        if (isHealthCheckServiceUp) {
            HealthCheckReport microServiceHealthReport = getServiceReport(microServicesToCheck);
            logger.info("Micro-Services Health Check Report, Healthy = " + microServiceHealthReport.getHealthy());
            microServiceHealthReport.report();

            if (!microServiceHealthReport.getHealthy()) {
                errorMessage = "Services health check failed! ";
            }
        } else {
            logger.info("Micro-Service Health Check Service Down, NOT able to do health check, will ignore");
        }

        // Oozie jobs health check.
        if (isOozieJobHealthCheckServiceUp) {
            HealthCheckReport oozieJobHealthReport = getOozieJobReport(oozieJobsToCheck);
            logger.info("Oozie Jobs Health Check Report, Healthy = " + oozieJobHealthReport.getHealthy());
            oozieJobHealthReport.report();

            if (!oozieJobHealthReport.getHealthy()) {
                errorMessage += "Oozie Job health check failed! ";
            }

        } else {
            logger.info("Oozie Job Health Check Service Down, NOT able to do health check, will ignore");
        }

        if (!QaStringUtils.isEmpty(errorMessage)) {
            throw new QaRuntimeException("Health Check Failed! " + errorMessage);
        }
    }

    public HealthCheckReport getOozieJobReport(List<ServiceEnum> oozieJobEnums) {

        HealthCheckReport healthReport = new HealthCheckReport();

        if (!isOozieJobHealthCheckServiceUp) {
            healthReport.setMessage("Oozie Health Check Service Not Up");
            return healthReport;
        }

        for (ServiceEnum jobEnum : oozieJobEnums) {
            HealthCheckReport jobHealthReport = getOozieJobReport(jobEnum);
            healthReport.setHealthy(healthReport.getHealthy() && jobHealthReport.getHealthy());
            healthReport.getReportItems().putAll(jobHealthReport.getReportItems());
        }

        return healthReport;
    }

    public HealthCheckReport getOozieJobReport(ServiceEnum oozieJobEnum) {

        HealthCheckReport healthCheckReport = new HealthCheckReport();
        if (!isOozieJobHealthCheckServiceUp) {
            healthCheckReport.setMessage("Oozie Health Check Service Not Up");
            return healthCheckReport;
        }

        HealthCheckReportItem jobReportItem = new HealthCheckReportItem();
        if (oozieJobsMap.get(oozieJobEnum) != null) {
            healthCheckReport.setHealthy(true);
            jobReportItem.setHealthy(true);
        } else {
            healthCheckReport.setHealthy(false);
            jobReportItem.setHealthy(false);
        }
        jobReportItem.setService(oozieJobEnum.getServiceName());
        healthCheckReport.getReportItems().put(oozieJobEnum.getServiceName(),jobReportItem);
        return healthCheckReport;
    }


    public HealthCheckReport getServiceReport(List<ServiceEnum> serviceEnums) {

        HealthCheckReport healthReport = new HealthCheckReport();
        for (ServiceEnum serviceEnum : serviceEnums) {

            HealthCheckReport subHealthReport = this.getServiceReport(serviceEnum);

            healthReport.setHealthy( healthReport.getHealthy() && subHealthReport.getHealthy());
            healthReport.getReportItems().putAll(subHealthReport.getReportItems());
        }

        return healthReport;
    }


    public HealthCheckReport getServiceReport(ServiceEnum serviceEnum) {

        HealthCheckReport healthReport = new HealthCheckReport();

        if (!this.serviceHostMap.containsKey(serviceEnum.getServiceName())) {
            logger.info(serviceEnum.getServiceName() + " is not a known service for Health Check");
            return healthReport;
        }

        List<String> serviceHosts = this.serviceHostMap.get(serviceEnum.getServiceName());
        for (String serviceHost : serviceHosts) {
            String url = serviceHealthCheckerUrl + "/api/app/health?hostname=" + serviceHost + "&service=" + serviceEnum.getServiceName();
            healthReport = this.getServiceReport(url);

            if (healthReport.getHealthy()) {
                return healthReport;
            }
        }

        return healthReport;
    }

    public HealthCheckReport getServiceReport(String healthCheckUrl) {

        Map<String, Object> response = restClient.getMap(healthCheckUrl);

        Map<String, Object> l2HealthCheckResults = (Map<String, Object>)response.get("L2HealthCheck");
        if (CollectionUtils.isEmpty(l2HealthCheckResults)) {
            l2HealthCheckResults = (Map<String, Object>)response.get("L1HealthCheck");
        }
        String serviceFullName = l2HealthCheckResults.keySet().toArray()[0].toString();

        HealthCheckReport healthReport = new HealthCheckReport();
        healthReport.setHealthy(Boolean.valueOf(response.get("Healthy").toString()));

        // the root service
        HealthCheckReportItem reportItem = new HealthCheckReportItem();
        reportItem.setService(serviceFullName);
        reportItem.setBranch(response.get("BranchName").toString());
        reportItem.setHealthy(Boolean.valueOf(response.get("Healthy").toString()));

        healthReport.getReportItems().put(serviceFullName, reportItem);

        // the dependency services: L2 Health Check
        List<String> keyChain = new ArrayList<>();
        keyChain.add(serviceFullName);

        keyChain.add("L2");
        Map<String, Object> l2HealthReport = getSubMap(l2HealthCheckResults, keyChain);
        for (String dependencyServiceName : l2HealthReport.keySet()) {
            Map<String, Object> dependencyServiceReport = (Map<String, Object>)l2HealthReport.get(dependencyServiceName);

            String serviceShortName = dependencyServiceName.replace("HealthCheck", "").replace("-health-check", "");
            if ("Version".equalsIgnoreCase(serviceShortName)) {
                continue;
            }

            HealthCheckReportItem dependencyItem = new HealthCheckReportItem();
            dependencyItem.setService(serviceShortName);
            dependencyItem.setHealthy(Boolean.valueOf(dependencyServiceReport.get("healthy").toString()));
            dependencyItem.setMessage(dependencyServiceReport.get("message").toString());

            healthReport.getReportItems().put(serviceShortName, dependencyItem);
        }

        return healthReport;

    }

    private Map<String, Object> getSubMap(Map<String, Object> parentMap, List<String> keyChain) {

        if (CollectionUtils.isEmpty(keyChain)) {
            return parentMap;
        }

        String nextKey = keyChain.remove(0);
        if (!parentMap.containsKey(nextKey)) {
            throw new QaRuntimeException("Failed in getting sub map by key " + nextKey);
        }

        Map<String, Object> subMap = (Map<String, Object>)parentMap.get(nextKey);
        if (CollectionUtils.isEmpty(keyChain)) {
            return subMap;
        }

        return getSubMap(subMap, keyChain);

    }

    public Map<String, List<String>> loadServiceStatus() {
        return loadServiceStatus(this.env);
    }

    public Map<String, List<String>> loadServiceStatus(String env) {
        Map<String, List<String>> serviceHostMap = new HashMap<>();

        String serviceListApiUrl = serviceHealthCheckerUrl + "/api/hosts/" + env;
        List<Map<String, Object>> maps = restClient.getMapList(serviceListApiUrl);
        for (Map<String, Object> map : maps) {
            String hostName = map.get("hostname").toString();
            String serviceName = map.get("service").toString();

            if (!serviceHostMap.containsKey(serviceName)) {
                serviceHostMap.put(serviceName, new ArrayList<>());
            }

            serviceHostMap.get(serviceName).add(hostName);
        }

        return serviceHostMap;
    }

    public Map<ServiceEnum, Map<String, Object>> loadOozieJobStatus() {
        return loadOozieJobStatus(this.env);
    }

    public Map<ServiceEnum, Map<String, Object>> loadOozieJobStatus(String environment) {

        String url = oozieJobHealthCheckUrl + "/v2/jobs?jobtype=coord&filter=status=RUNNING&timezone=GMT";
        Map<String, Object> responseMap = restClient.getMap(url);

        List<Map<String, Object>> jobList = (List<Map<String, Object>>) responseMap.get("coordinatorjobs");

        for (Map<String, Object> job : jobList) {
            String name = job.get("coordJobName").toString();
            String jobPath = job.get("coordJobPath").toString();

            ServiceEnum jobName = ServiceEnum.getFromString(name);
            if (jobName != null && jobPath.contains(environment)) {
                oozieJobsMap.put(jobName, job);
            }
        }
        return oozieJobsMap;
    }

}