package com.automatos.qa.common.integration.fojos;

import com.automatos.qa.commoncore.exception.QaRuntimeException;
import org.junit.rules.ExternalResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.FileNotFoundException;

/**
 * Provides a managed Spring Context for testing. This class should be used as a JUnit4 @ClassRule. JUnit will start and
 * close the Spring Context automatically.
 * <p/>
 * If multiple instances or subclasses of this class are used in a JUnit class hierarchy they will each have separate
 * spring contexts. This allows for independent use of spring by different code layers without interference in the
 * contexts lifecycle.
 */
public class SpringResource extends ExternalResource {

    private static final Logger log = LoggerFactory.getLogger(SpringResource.class);

    private ClassPathXmlApplicationContext springContext;
    private String[] configs;
    private String name;
    private boolean initialized = false;

   public SpringResource(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public SpringResource setConfigs(String... configs) {
        this.configs = configs;
        return this;
    }

    public ClassPathXmlApplicationContext ctx() {
           return springContext;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void doBefore() {
        before();
    }

    public void doAfter() {
        after();
    }

    @Override
    protected void before() {
        log.info("Initializing '" + name + "'");
        try {
            springContext = new ClassPathXmlApplicationContext(configs);
            initialized = true;
            //log.info("Initialized resource: '{}'", name);
        } catch (BeanDefinitionStoreException bds) {

            // log but proceed if we are missing properties to use service
            if (bds.getMessage().contains("Could not resolve placeholder")) {
                // this happens for setter based injection
                log.warn("Required env property for {} does not exist.  The resource will not be available.\n{}", name, bds.getMessage());
            }
            else if (bds.getMessage().contains("Ambiguous constructor argument types")) {
                // this happens for constructor injection
                log.warn("Required env property for {} does not exist.  The resource will not be available.\n{}", name, bds.getMessage());
            } else {
                throw bds;
            }
        } catch (BeanInitializationException bie) {
            if (bie.getCause() != null && bie.getCause() instanceof FileNotFoundException) {
                log.warn("env.properties not found for resource {}.  The resource will not be available.\n{}", name, bie.getCause().getMessage());

            }
        } catch (BeanCreationException e) {
            
            //Check to see if we had a QaRuntimeException which currently only can happen if user fails
            //the UnmodifiableProperties unique property name check and throw the error if found
            Throwable cause = e.getCause();
            Throwable lastKnownCause = e;
            while (cause != null) {
                lastKnownCause = cause;
                if (cause.getCause() instanceof QaRuntimeException) {
                    throw new QaRuntimeException(cause);
                }
                cause = cause.getCause();
            }
            
            //Some other BeanCreationException occurred, log and move on.
            log.warn("There was a problem instantiating '{}' resource. The resource will not be available.\n{}.  Root causes exception: {}", name, e.getMessage(), lastKnownCause.getMessage());
        }
    }

    @Override
    protected void after() {
        try {
            if (springContext != null) {
                springContext.close();
                //log.info("Closed resource: '{}'", name);
            }
        } catch (Exception e) {
            log.info("Failed to close resource: '{}'", name);
        }
    }
}
