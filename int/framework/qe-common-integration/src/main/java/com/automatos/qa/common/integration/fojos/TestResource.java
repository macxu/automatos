package com.automatos.qa.common.integration.fojos;

import com.automatos.qa.common.integration.env.TestEnv;
import com.automatos.qa.common.integration.env.TestSharedEnvProperties;
import com.automatos.qa.common.integration.fojos.stringservice.StringServiceFojo;
import com.automatos.qa.commoncore.properties.QaProperties;
import com.automatos.qa.commoncore.exception.QaRuntimeException;
import org.junit.rules.ExternalResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Provides a central point of access for shared Resources for QA Integration Tests
 * Access in tests via JUnit4 @ClassRule.
 * This class is NOT thread safe due to the TestEnv switching with static flags that can occur.
 * Do not run tests using multiple env settings in a multithreaded manner.
 *
 */
public class TestResource extends ExternalResource implements FojoInterface {

    private static final Logger log = LoggerFactory.getLogger(TestResource.class);

    private TestEnv qaEnv;
    private TestSharedEnvProperties testSharedEnvProperties;
    private String testEnvKey;

    public TestResource(String targetMarinEnvKey) {
        testSharedEnvProperties = new TestSharedEnvProperties();

        this.testEnvKey = targetMarinEnvKey;
        qaEnv = TestEnv.getInstance();
        reinitializeMarinEnv();
    }

    public TestResource() {
        this(TestEnv.TEST_ENV_KEY);
    }

    protected void registerServices() {

        // register all the FOJO's here
        registerService("stringServiceFojo", "String Service", "/conf/stringservice/beans.xml");
    }

    public TestEnv getEnv() {
        return qaEnv;
    }

    public String getTargetMarinEnvKey() {
        return testEnvKey;
    }

    public void reinitializeMarinEnv() {
        qaEnv.setActiveEnvKey(testEnvKey);
        qaEnv = TestEnv.getInstance();
    }

    public TestSharedEnvProperties sharedEnvProperties() {
        return testSharedEnvProperties;
    }


    public StringServiceFojo stringService() {
        return getFojo("stringServiceFojo", StringServiceFojo.class);
    }


    @Override
    public void before() {
        log.info("Initializing Marin Resources.");

        reinitializeMarinEnv();
        registerServices();
        initializeResouces();

        log.info("Initialization of MarinResources finished.");
    }

    @Override
    public void after() {
        log.info("Closing all resources of MarinResources.");

        closeResources();
        registeredServices.clear();

        log.info("Closed all resources of MarinResources.");
    }

    /**
     * Find the framework property file based on scope and name.
     *
     * @param scope name of the property bundle directory to search in
     * @param name  name of the property file
     * @return {@link QaProperties}
     */
    public QaProperties getPropertyBundle(String scope, String name) {

        reinitializeMarinEnv();

        QaProperties properties = new QaProperties();
        String path = qaEnv.getFrameworkResourcePath(scope, name);

        InputStream inputStream = TestResource.class.getResourceAsStream(path);

        if (inputStream == null) {

            throw new QaRuntimeException("Property file does not exist at scope: " + scope + " name: " + name);
        }

        try {

            properties.load(inputStream);
        } catch (IOException e) {

            throw new QaRuntimeException("Unable to load property file scope: " + scope + " name: " + name);
        }

        return properties;
    }
}
