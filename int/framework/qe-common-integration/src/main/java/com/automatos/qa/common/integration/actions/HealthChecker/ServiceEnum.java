package com.automatos.qa.common.integration.actions.HealthChecker;

/**
 * Service Enums
 */
public enum ServiceEnum {

    TDS("tds");

    private String serviceName;

    private ServiceEnum(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return the name of a service
     */
    public String getServiceName() {
        return this.serviceName;
    }

    public static ServiceEnum getFromString(String ozzieJobName) {
        for (ServiceEnum jobEnum : ServiceEnum.values()) {
            if (jobEnum.getServiceName().equalsIgnoreCase(ozzieJobName)) {
                return jobEnum;
            }
        }

        return null;
    }

}
