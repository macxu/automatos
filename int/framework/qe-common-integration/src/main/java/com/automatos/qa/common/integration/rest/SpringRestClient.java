package com.automatos.qa.common.integration.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.automatos.qa.commoncore.url.QaURL;
import com.automatos.qa.commoncore.exception.QaRuntimeException;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.CommonsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Class for performing simple rest calls over http
 *
 */
public class SpringRestClient {
    private static final Logger log = LoggerFactory.getLogger(SpringRestClient.class);
    
    private int readTimeoutInSeconds = 120;
    private int connectionTimeoutInSeconds = 30;
    private RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());
    
    public void setConnectionTimeout(int connectionTimeoutInSeconds) {
        this.connectionTimeoutInSeconds = connectionTimeoutInSeconds;
        restTemplate = new RestTemplate(clientHttpRequestFactory());
    }
    
    public void setReadTimeout(int readTimeoutInSeconds) {
        this.readTimeoutInSeconds = readTimeoutInSeconds;
        restTemplate = new RestTemplate(clientHttpRequestFactory());
    }
    
    /**
     * Makes a get request using the provided url and params
     * @param url - the url to send the get request to
     * @param returnType the class type to return the results as
     * @param params - the params to append on the url
     */
    public <T> T get(QaURL url, Class<T> returnType, Map<String, Object> params) {
        try {
            log.trace("Making get request to: {}", url.getUrl().toString());
            return restTemplate.getForObject(url.getUrl().toString(), returnType, params);
        }
        catch (HttpClientErrorException e) {
            log.error(e.getResponseBodyAsString());
            throw new QaRuntimeException(e.getResponseBodyAsString(), e);
        }
    }

    /**
     * Makes a get request using the provided url and variables. URI Template variables are expanded using the given URI params.
     * @param url the URL
     * @param returnType the class type to return the results as
     * @param params the variables to expand the template
     * @return the converted object
     */
    public <T> T get(QaURL url, Class<T> returnType, String params) {
        try {
            log.trace("Making get request to: {}", url.getUrl().toString());
            return restTemplate.getForObject(url.getUrl().toString(), returnType, params);
        } catch (HttpClientErrorException e) {
            log.error(e.getResponseBodyAsString());
            throw new QaRuntimeException(e.getResponseBodyAsString(), e);
        }
    }
    
    public <T> T get(QaURL url, Class<T> classType) {
        return get(url, classType, new HashMap<>());
    }
    
    public <T> T get(String url, Class<T> classType, Map<String, Object> params) {
        return get(new QaURL(url), classType, params);
    }
    
    public <T> T get(String url, Class<T> classType) {
        return get(new QaURL(url), classType);
    }

    public String getString(String url) {
        return get(new QaURL(url), String.class);
    }

    public JSONObject getJSONObject(String url) {
        return get(new QaURL(url), JSONObject.class);
    }

    public List<Map<String, Object>> getMapList(String url) {

        String response = this.getString(url);

        ObjectMapper mapper = new ObjectMapper();
        try {
            List<Map<String, Object>> maps = mapper.readValue(response, new TypeReference<List<Map<String, Object>>>() {});

            return maps;
        } catch (IOException e) {
            throw new QaRuntimeException("Couldn't convert response to List of Maps", e);
        }
    }

    public Map<String, Object> getMap(String url) {

        String response = this.getString(url);

        ObjectMapper mapper = new ObjectMapper();
        try {
            Map<String, Object> map = mapper.readValue(response, new TypeReference<Map<String, Object>>() {});

            return map;
        } catch (IOException e) {
            throw new QaRuntimeException("Couldn't convert response to a map", e);
        }
    }

    /**
     * Makes a post request using the provided url, object, and params
     * @param url - the url to send the post request to
     * @param objectToPost - the object to send with the post request
     * @param params - the params to append to the post request
     * @return - A String representing the response from the remote system
     */
    public <T> T post(QaURL url, Object objectToPost, Class<T> returnType, Map<String, Object> params) {
        try {
            return restTemplate.postForObject(url.getUrl().toString(), objectToPost, returnType, params);
        }
        catch (HttpClientErrorException e) {
            log.error(e.getResponseBodyAsString());
            throw new QaRuntimeException(e.getResponseBodyAsString(), e);
        }
    }

    public <T> T post(QaURL url, Object objectToPost, Class<T> returnType) {
        return post(url, objectToPost, returnType, new HashMap<>());
    }
    
    public <T> T post(String url, Object objectToPost, Class<T> returnType, Map<String, Object> params) {
        return post(new QaURL(url), objectToPost, returnType, params);
    }
    
    public <T> T post(String url, Object objectToPost, Class<T> returnType) {
        return post(new QaURL(url), objectToPost, returnType);
    }
    
    public <T> T postJson(QaURL url, String jsonDataToPostAsString, Class<T> returnType, Map<String, Object> params) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(jsonDataToPostAsString, headers);
        
        return post(url, entity, returnType, params);
    }
    
    public <T> T postJson(QaURL url, JSONObject jsonDataToPost, Class<T> returnType, Map<String, Object> params) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        List<MediaType> acceptedMediaTypes = new ArrayList<>();
        acceptedMediaTypes.add(MediaType.APPLICATION_JSON);
        headers.setAccept(acceptedMediaTypes);
        HttpEntity<String> entity = new HttpEntity<>(jsonDataToPost.toString(), headers);
        
        return post(url, entity, returnType, params);
    }
    
    public <T> T postJson(QaURL url, String jsonDataToPostAsString, Class<T> returnType) {
        return postJson(url, jsonDataToPostAsString, returnType, new HashMap<>());
    }
    
    public String postJson(QaURL url, String jsonDataToPostAsString) {
        return postJson(url, jsonDataToPostAsString, String.class, new HashMap<>());
    }

    public JSONObject putJson(QaURL url, JSONObject jsonDataToPost) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        List<MediaType> acceptedMediaTypes = new ArrayList<>();
        acceptedMediaTypes.add(MediaType.APPLICATION_JSON);
        headers.setAccept(acceptedMediaTypes);
        HttpEntity<String> entity = new HttpEntity<>(jsonDataToPost.toString(), headers);

        ResponseEntity<String> response = restTemplate.exchange(url.getUri(), HttpMethod.PUT, entity, String.class);

        JSONObject responseObject = null;
        try {
            responseObject = new JSONObject(response.getBody());
            responseObject.put("statusCode", response.getStatusCode());
        } catch (JSONException e) {
            throw new QaRuntimeException("Put failed, see log for server response", e);
        }

        return responseObject;
    }
    
    public void put(QaURL url, Object objectToPut) {
        try {
            restTemplate.put(url.getUrl().toString(), objectToPut, new HashMap<>());
        }
        catch (HttpClientErrorException e) {
            log.error("Put failed: " + e.getResponseBodyAsString());
            throw new QaRuntimeException("Put failed, see log for server response", e);
        }
    }

    /**
     * Makes a delete request using the provided url and params
     * @param url - the url to send the delete request to
     * @param params - the params to append to the delete request
     */
    public void delete(QaURL url, Map<String, Object> params) {
        try {
            restTemplate.delete(url.getUrl().toString(), params);
        }
        catch (HttpClientErrorException e) {
            log.error("Delete Failed: " + e.getResponseBodyAsString());
            throw new QaRuntimeException(e.getResponseBodyAsString(), e);
        }
    }

    public void delete(QaURL url) {
        delete(url, new HashMap<>());
    }

    //set the timeouts to (Connection: 30s, Read: 500s)
    private ClientHttpRequestFactory clientHttpRequestFactory() {
        CommonsClientHttpRequestFactory factory = new CommonsClientHttpRequestFactory();
        factory.setReadTimeout((int)TimeUnit.SECONDS.toMillis(readTimeoutInSeconds));
        factory.setConnectTimeout((int)TimeUnit.SECONDS.toMillis(connectionTimeoutInSeconds));
        return factory;
    }

    public <T> ResponseEntity<T> exchange(QaURL url, HttpMethod httpMethod, HttpEntity<?> requestEntity, Class<T> returnType) {
        try {
            return restTemplate.exchange(url.getUri(), httpMethod, requestEntity, returnType);
        } catch (HttpClientErrorException e) {
//            log.error(e.getResponseBodyAsString());
            if (e.getStatusCode().value() == 400) {
                return new ResponseEntity(e.getResponseBodyAsString(), HttpStatus.BAD_REQUEST);
            } else if (e.getStatusCode().value() == 401) {
                return new ResponseEntity(e.getResponseBodyAsString(), HttpStatus.UNAUTHORIZED);
            } else if (e.getStatusCode().value() == 404) {
                return new ResponseEntity(e.getResponseBodyAsString(), HttpStatus.NOT_FOUND);
            } else {
                throw new QaRuntimeException(e.getResponseBodyAsString(), e);
            }
        }
    }
}
