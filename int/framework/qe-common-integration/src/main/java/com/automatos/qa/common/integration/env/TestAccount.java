package com.automatos.qa.common.integration.env;

import com.automatos.qa.common.integration.enums.PublisherEnum;

/**
 * A representation of a registered pca
 *
 * Created by mxu on 5/18/15.
 */
public class TestAccount {

    private PublisherEnum publisher;
    private String username;
    private String password;
    private String accountId;
    private String accessToken;
    private String refreshToken;
    private String apiUrl;
    private String description;
    private String environment;
    private String publisherDefinitionName;
    private String authType;
    private String clientId;
    private String clientSecret;
    private boolean needLinkToMarinMcc;

    public PublisherEnum getPublisher() {
        return publisher;
    }

    public void setPublisher(PublisherEnum publisher) {
        this.publisher = publisher;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getPublisherDefinitionName() {
        return publisherDefinitionName;
    }

    public void setPublisherDefinitionName(String publisherDefinitionName) {
        this.publisherDefinitionName = publisherDefinitionName;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public boolean getNeedLinkToMarinMcc() {
        return needLinkToMarinMcc;
    }

    public void setNeedLinkToMarinMcc(boolean needLinkToMarinMcc) {
        this.needLinkToMarinMcc = needLinkToMarinMcc;
    }

    public String getClientId() {return clientId;}

    public String getClientSecret() {return clientSecret;}

    /**
     * There are cases where we need a key to identify a PCA, either in logging, or in giving nickname of a PCA.
     * but, for some publishers like Google and Bing, their PCA's have account ID, but no username.
     * for publishers like Baidu and Yandex, their PCAs have username, but no account ID.
     */
    public String getAccountKey() {
        if (accountId != null) {
            return accountId;
        }

        return username;
    }
    
    @Override
    public String toString() {
        return getPublisher() + ": " + getAccountKey();
    }
}
