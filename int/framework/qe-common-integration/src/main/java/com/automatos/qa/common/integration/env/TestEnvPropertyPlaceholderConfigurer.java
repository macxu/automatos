package com.automatos.qa.common.integration.env;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.Properties;

/**
 * Class to help Spring load a [marin.env].properties resource. Uses {@link TestEnv} to find
 * the current marin environment specified.  Also used to resolve framework properties from Spring.
 *
 * This class also gives us a hook point to do anything we want with spring properties as we resolve them in the future.
 */
public class TestEnvPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

    // the scope for framework properties
    private String scope;

    // the name for framework properties
    private String name;

    @Override
    protected void loadProperties(Properties props) throws IOException {

        ClassPathResource classPathResource;

        TestEnv marinEnv = TestEnv.getInstance();
        marinEnv.loadProperties(props);

        // if scope is provided we are loading framework properties
        if(scope != null) {
            classPathResource = new ClassPathResource(marinEnv.getFrameworkResourcePath(scope, name));

            super.setLocation(classPathResource);
            super.loadProperties(props);
        }
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public void setName(String name) {
        this.name = name;
    }
}
