package com.automatos.qa.common.integration.actions.HealthChecker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class HealthCheckReport {

    private static final Logger logger = LoggerFactory.getLogger(HealthCheckerFojo.class);

    private Boolean isHealthy;
    private String message;
    private Map<String, HealthCheckReportItem> reportItems;

    public HealthCheckReport() {
        this.isHealthy = true;
        reportItems = new HashMap<>();
    }

    public Boolean getHealthy() {
        return isHealthy;
    }

    public void setHealthy(Boolean healthy) {
        isHealthy = healthy;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, HealthCheckReportItem> getReportItems() {
        return reportItems;
    }

    public void setReportItems(Map<String, HealthCheckReportItem> reportItems) {
        this.reportItems = reportItems;
    }

    public void report() {
        for (String dependencyService : reportItems.keySet()) {
            HealthCheckReportItem reportItem = reportItems.get(dependencyService);
            logger.info("\t" + dependencyService + ": " + reportItem.getHealthy());

            if (!reportItem.getHealthy()) {
                logger.info("\t\t" + message);
            }
        }
     }
}
