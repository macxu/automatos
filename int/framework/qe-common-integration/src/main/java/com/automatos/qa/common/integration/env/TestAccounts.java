package com.automatos.qa.common.integration.env;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.automatos.qa.common.integration.enums.PublisherEnum;
import com.automatos.qa.common.integration.fojos.TestResource;
import com.automatos.qa.commoncore.exception.QaRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A representation of the registered pcas
 *
 * Created by mxu on 5/18/15.
 */
public class TestAccounts {

    private static final Logger log = LoggerFactory.getLogger(TestAccounts.class);

    private static final String PCAS_FILE = "/conf/env/marin-pcas.json";

    private Map<String, Map<String, Map<String, TestAccount>>> registeredPcas;

    /**
     * Construct the object and load the registered pcas.
     *
     */
    public TestAccounts() {

        ObjectMapper mapper = new ObjectMapper();

        try {
            InputStream inputStream = TestResource.class.getResourceAsStream(PCAS_FILE);
            registeredPcas = mapper.readValue(inputStream, new TypeReference<Map<String, Map<String, Map<String, TestAccount>>>>(){});
        } catch (IOException e) {
            throw new QaRuntimeException(e, "Failed to load '{}'", PCAS_FILE);
        }
    }

    /**
     * Get the TestAccount object based on the given conditions.
     *
     * @param PublisherEnum the publisher of the pca
     * @param category the category of use of the pca, like "bulk", "voservice" etc.
     *
     * @return TestAccount
     */
    public TestAccount getPca(PublisherEnum publisherEnum, String category) {
        TestEnv marinEnv = TestEnv.getInstance();

        String envKey = marinEnv.toString();
        if (marinEnv.isVM()) {
            envKey = "vm";
        }

        log.info("Getting PCA info for '{}' '{}'", publisherEnum.name(), category);

        if (!registeredPcas.containsKey(publisherEnum.name().toLowerCase())) {
            throw new QaRuntimeException("No '{}' PCA is registered", publisherEnum.name());
        }

        if (!registeredPcas.get(publisherEnum.name().toLowerCase()).containsKey(category.toLowerCase())) {
            throw new QaRuntimeException("No category named '{}' for '{}' PCA is registered", category, publisherEnum.name());
        }

        if (!registeredPcas.get(publisherEnum.name().toLowerCase()).get(category.toLowerCase()).containsKey(envKey)) {
            throw new QaRuntimeException("No category named '{}' for '{}' PCA against '{}' is registered", category, publisherEnum.name(), envKey);
        }

        TestAccount marinPca = registeredPcas.get(publisherEnum.name().toLowerCase()).get(category.toLowerCase()).get(envKey);
        marinPca.setPublisher(publisherEnum);

        log.debug("PCA description: {}", marinPca.getDescription());
        log.debug("Account ID: {}", marinPca.getAccountId());
        log.debug("User Name: {}", marinPca.getUsername());
        log.debug("Password: {}", marinPca.getPassword());
        log.debug("Access Token: {}", marinPca.getAccessToken());
        log.debug("Refresh Token: {}", marinPca.getRefreshToken());

        return marinPca;
    }

    public List<TestAccount> getAllRegisteredPcas() {

        List<TestAccount> marinPcas = new ArrayList<>();

        for (Map.Entry<String, Map<String, Map<String, TestAccount>>> publisherEntry : registeredPcas.entrySet()) {

            Map<String, Map<String, TestAccount>> categoryRootEntry = publisherEntry.getValue();
            for (Map.Entry<String, Map<String, TestAccount>> categoryEntry : categoryRootEntry.entrySet()) {

                Map<String, TestAccount> environmentRootEntry = categoryEntry.getValue();

                for (Map.Entry<String, TestAccount> pcaEntry : environmentRootEntry.entrySet()) {

                    TestAccount marinPca = pcaEntry.getValue();

                    PublisherEnum publisherEnum = PublisherEnum.fromName(publisherEntry.getKey().toUpperCase());
                    marinPca.setPublisher(publisherEnum);
                    marinPca.setEnvironment(pcaEntry.getKey());

                    // add to the list to be returned
                    marinPcas.add(marinPca);
                }
            }
        }

        return marinPcas;
    }

}
