package com.automatos.qa.common.integration.actions;

import com.automatos.qa.common.integration.actions.HealthChecker.HealthCheckerFojo;
import com.automatos.qa.common.integration.fojos.TestResource;

/**
 * Class allowing access to all marin pipeline actions.
 *
 */
public class TestActions {

    private HealthCheckerFojo healthCheckerFojo;

    public TestActions(TestResource resources) {
        healthCheckerFojo = new HealthCheckerFojo(resources);
    }

    public HealthCheckerFojo healthChecker() {
        return healthCheckerFojo;
    }

}
