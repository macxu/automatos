package com.automatos.qa.common.integration.testdefinition;

import com.automatos.qa.commoncore.exception.QaRuntimeException;
import com.automatos.qa.commoncore.junit.mapper.JsonMapper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import org.apache.commons.io.IOUtils;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that explains how the {@link JsonMapper} is supposed to
 * match on the list of {@link BaseTestDefinition} contained
 * in the input files of a test.
 *
 */

public class BaseTestDefinitionMapper extends JsonMapper {

    @Override
    public TypeReference getValueTypeReference() {
        return new TypeReference<List<BaseTestDefinition>>() {
        };
    }

    /**
     * Maps an input file structured as JSON into a list of objects
     * that will then be passed individually to a test method via JUnitParams.
     * The items that are not AUTOMATED will be skipped
     *
     * @param reader Reader on the input file
     * @return Array of objects representing each test case
     */
    @Override
    public Object[] map(Reader reader) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JodaModule());
        try {
            List<Object> results = mapper.readValue(reader, getValueTypeReference());

            List<Object> automatedCases = new ArrayList<>();
            for (Object result : results) {
                BaseTestDefinition testDefinition = (BaseTestDefinition)result;

                if (testDefinition.isAutomated()) {
                    automatedCases.add(result);
                }
            }

            return automatedCases.toArray();

        } catch (Exception e) {
            throw new QaRuntimeException(e);
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }
}
