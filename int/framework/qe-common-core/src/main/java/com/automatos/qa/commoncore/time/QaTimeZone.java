package com.automatos.qa.commoncore.time;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

import java.util.TimeZone;

public class QaTimeZone {
    public static final TimeZone US_PACIFIC = TimeZone.getTimeZone("US/Pacific");
    public static final TimeZone US_EASTERN = TimeZone.getTimeZone("US/Eastern");

    private QaTimeZone() {
        throw new QaRuntimeException("Utility class");
    }
}
