package com.automatos.qa.commoncore.junit.mapper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.automatos.qa.commoncore.exception.QaRuntimeException;
import junitparams.mappers.DataMapper;
import org.apache.commons.io.IOUtils;

import java.io.Reader;
import java.util.List;

/**
 * JSON version of JUnitParams {@link DataMapper}. This allows a test
 * to define an input file in JSON format. To use, simply subclass this
 * and define what type of object structure is contained within the input files
 * by overriding {@link #getValueTypeReference()}
 *
 */
public abstract class JsonMapper implements DataMapper {

    /**
     * Maps an input file structured as JSON into a list of objects
     * that will then be passed individually to a test method via JUnitParams.
     *
     * @param reader Reader on the input file
     * @return Array of objects representing each test case
     */
    @Override
    public Object[] map(Reader reader) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JodaModule());
        try {
            List<Object> results = mapper.readValue(reader, getValueTypeReference());
            return results.toArray();
        } catch (Exception e) {
            throw new QaRuntimeException(e);
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }

    /**
     * Subclasses override this to influence how the mapper
     * should respond to a particular tests' input files.
     *
     * @return a TypeReference object conforming to the structure of
     * the input file
     */
    public abstract TypeReference getValueTypeReference();
}