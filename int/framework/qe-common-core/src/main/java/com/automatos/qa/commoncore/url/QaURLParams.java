package com.automatos.qa.commoncore.url;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Utility class for constructing the URL parameter String.
 *
 */
public class QaURLParams {

    public static final String NAME_VALUE_SEPARATOR = "=";
    public static final String PARAMETER_SEPARATOR = "&";
    public static final String ENCODING_UTF_8 = "UTF-8";

    private String urlParamString;

    /**
     * Joins url params into a query string for HTTP requests.
     *
     * Example:
     * urlParams: "isForceSync", "true", "pcaId", "101"
     * joined params: isForceSync=true&pcaId=101
     *
     * @param urlParams param list of <paramKey, paramValue>
     */
    public QaURLParams(String... urlParams) {

        if (urlParams.length % 2 != 0) {
            throw new QaRuntimeException("Need an even number of parameters");
        }

        // Build a list of <paramKey1=paramValue1, paramKey2=paramValue2, ...>
        List<String> params = new ArrayList<>();
        for (int i = 0; i < urlParams.length; i += 2) {
            String param = urlParams[i] + NAME_VALUE_SEPARATOR + encodeUTF8(urlParams[i + 1]);
            params.add(param);
        }

        // Join the list of params into string of format: "paramKey1=paramValue1&paramKey2=paramValue2..."
        this.urlParamString = StringUtils.join(params, PARAMETER_SEPARATOR);
    }

    /**
     * Join url params into a query string for HTTP requests.
     *
     * @param urlParams param Map of <paramKey, paramValue>
     */
    public QaURLParams(Map<String, Object> urlParams) {
        List<String> params = new ArrayList<>();
        for (String param : urlParams.keySet()) {
            params.add(param + NAME_VALUE_SEPARATOR + encodeUTF8(urlParams.get(param)));
        }

        // Join the list of params into string of format: "paramKey1=paramValue1&paramKey2=paramValue2..."
        this.urlParamString = StringUtils.join(params, PARAMETER_SEPARATOR);
    }

    public String getUrlParamString() {
        return urlParamString;
    }

    /**
     * Encode param using UTF-8 encoding. Mostly you want this method.
     * @param rawParam param to be encoded
     * @return UTF-8 encoded version of the input object, toString'd
     */
    public static String encodeUTF8(Object rawParam) {
        return encode(rawParam, ENCODING_UTF_8);
    }

    /**
     * URL encode params using the specified encoding. Mostly you want to use UTF-8 (see encodeUTF8)
     *
     * @param rawParam param to be encoded
     * @param encoding encoding to use
     * @return encoded version of the input object, toString'd
     */
    public static String encode(Object rawParam, String encoding) {
        String encodedParam;

        try {
            encodedParam = URLEncoder.encode(rawParam.toString(), encoding);
        } catch (UnsupportedEncodingException uee) {
            // this should never happen
            throw new QaRuntimeException(uee, "Cannot encode parameter '{}' using '{}'", rawParam.toString(), encoding);
        }

        return encodedParam;
    }

}
