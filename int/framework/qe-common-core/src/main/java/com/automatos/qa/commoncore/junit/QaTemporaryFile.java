package com.automatos.qa.commoncore.junit;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;

/**
 * This class provides a way manage temporary files for tests and retain them for debugging.
 * The temporary files will be created in the maven target directory under a class specific directory.
 * If not run via maven, they will be created in the user working directory under target.
 * <p/>
 * This is a modified copy of the JUnit QaTemporaryFolder which was not suitable for subclassing.
 * This class is not intended to be used as a JUnit Rule or ClassRule.
 *
 * @see <a href='http://junit.org/javadoc/4.11/org/junit/rules/TemporaryFolder.html'>TemporaryFolder</a>
 */
public class QaTemporaryFile {

    private static final Logger log = LoggerFactory.getLogger(QaTemporaryFile.class);
    private static final SecureRandom random = new SecureRandom();

    // ultimately the target directory
    private final File parentFolder;

    // the class specific directory
    private File folder;

    // used to get the classname
    private Class classUnderTest;

    public QaTemporaryFile(Class classUnderTest) {

        this.classUnderTest = classUnderTest;

        // are we using maven?
        String workspaceRoot = System.getProperty("maven.project.build.directory");

        // otherwise use working directory
        if (workspaceRoot == null) {

            workspaceRoot = System.getProperty("user.dir");

            // pretend maven, so things are always in the same place
            workspaceRoot += File.separator + "target" + File.separator + "temp";
            log.trace("Root based on user.dir");
        } else {
            workspaceRoot += File.separator + "temp";
            log.trace("Root based on maven project.build.directory");
        }

        File file = new File(workspaceRoot);

        file.mkdirs();

        parentFolder = file;

        create();
    }

    private void create() {

        folder = createWorkingFolderIn(parentFolder);
        log.trace("Temporary file directory located at '{}'", folder.getAbsolutePath());

    }

    /**
     * Create a tmpFile for the inputStream
     * @param inputStream input stream
     * @return tmp file of the inputStream
     */
    public File newFileFromStream(InputStream inputStream) {
        File tmpFile = newFile();
        try (OutputStream outputStream = new FileOutputStream(tmpFile)){
            IOUtils.copy(inputStream, outputStream);
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
        return tmpFile;
    }

    /**
     * Returns a new fresh file with the given name under the temporary folder.
     */
    public File newFile(String fileName) {
        File file = new File(getRoot(), fileName);
        try {
            if (!file.createNewFile()) {
                throw new QaRuntimeException("a file with the name \'" + fileName + "\' already exists in the test folder");
            }
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        }

        log.info("Created '{}'", file.getAbsolutePath());
        return file;
    }

    /**
     * Returns a new fresh file with a random name under the temporary folder.
     */
    public File newFile() {
        try {
            String name = getRandomName();
            File file = new File(folder, name);

            if (file.exists()) {
                throw new QaRuntimeException("a file with the name \'" + name + "\' already exists");
            }

            file.createNewFile();

            log.info("Created '{}'", file.getAbsolutePath());
            return file;

        } catch (IOException e) {
            throw new QaRuntimeException(e);
        }
    }

    /**
     * Copied from java File source.
     *
     * @return
     */
    private String getRandomName() {
        long n = random.nextLong();
        if (n == Long.MIN_VALUE) {
            n = 0;
        } else {
            n = Math.abs(n);
        }
        return Long.toString(n);
    }

    private boolean isLastElementInArray(int index, String[] array) {
        return index == array.length - 1;
    }

    private File createWorkingFolderIn(File parentFolder) {
        File createdFolder;

        createdFolder = new File(parentFolder, classUnderTest.getName());

        createdFolder.mkdir();

        return createdFolder;
    }

    /**
     * @return the location of this temporary folder.
     */
    public File getRoot() {
        if (folder == null) {
            throw new QaRuntimeException("the temporary folder has not yet been created");
        }
        return folder;
    }

    public void cleanRootDirectory() {
        try {
            FileUtils.cleanDirectory(getRoot());          
        } catch (IOException e) {
            throw new QaRuntimeException("Unable to clean root directory", e);
        }
    }
}
