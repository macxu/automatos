package com.automatos.qa.commoncore.http;

import com.automatos.qa.commoncore.exception.QaRuntimeException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Class for http related methods.
 *
 * @deprecated Please use {@link SpringRestClient} in qa-common-integration
 */
@Deprecated
public class QaHttpClient {

    private static final Logger logger = LoggerFactory.getLogger(QaHttpClient.class);
    private DefaultHttpClient defaultHttpClient;
    private BasicCookieStore cookies;
    private int httpTimeoutInSeconds;

    /**
     * Initializes the httpclient
     */
    public void init() {
        defaultHttpClient = new DefaultHttpClient();
        httpTimeoutInSeconds = 60;

        defaultHttpClient.getParams().setParameter("http.socket.timeout", httpTimeoutInSeconds * 1000);
    }

    /**
     * Cleans up the httpclient
     */
    public void destroy() {
        defaultHttpClient.getConnectionManager().shutdown();
    }

    public void setCookies(BasicCookieStore cookies) {
        this.cookies = cookies;
    }

    public int getHttpTimeoutInSeconds() {
        return httpTimeoutInSeconds;
    }

    /**
     * Sets the timeout of getHttpResponse(HttpPost httpPost) only
     * @param httpTimeoutInSeconds the timeout value in seconds
     */
    public void setHttpTimeoutInSeconds(int httpTimeoutInSeconds) {
        this.httpTimeoutInSeconds = httpTimeoutInSeconds;

        defaultHttpClient.getParams().setParameter("http.socket.timeout", httpTimeoutInSeconds * 1000);
    }
   
    /**
     * Gets the http response based on the request url
     *
     * @param requestUrl - the request url that attempts to get the file content
     * @return - the httpresponse that will provide the contents of the output file
     */
    public HttpResponse getHttpResponse(URI requestUrl) {

        HttpResponse response;
        HttpGet getRequest = new HttpGet(requestUrl);

        try {
            if (cookies != null) {
                BasicHttpContext localContext = new BasicHttpContext();
                localContext.setAttribute(ClientContext.COOKIE_STORE, cookies);
                response = defaultHttpClient.execute(getRequest, localContext);
            } else {
                response = defaultHttpClient.execute(getRequest);
            }
        } catch (IOException e) {
            throw new QaRuntimeException("There is a problem with the getPostResponse request. ", e);
        }

        return response;
    }

    /**
     * Gets the http response based on a post request in JSONObject format.
     *
     * @param url - the request URL.
     * @param postEntity - a post request in JSONObject format.
     * @return the httpresponse that will provide the contents of the output file
     */
    public HttpResponse getHttpPostResponse(URI url, JSONObject postEntity) {

        HttpResponse response = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            StringEntity se = new StringEntity(postEntity.toString());
            se.setContentType(ContentType.APPLICATION_JSON.toString());
            httpPost.setEntity(se);
            response = defaultHttpClient.execute(httpPost);
        } catch (UnsupportedEncodingException e) {
            throw new QaRuntimeException("There is a problem with the getPostResponse request. ", e);
        } catch (IOException e) {
            throw new QaRuntimeException("There is a problem with the getPostResponse request. ", e);
        }
        return response;
    }

    /**
     * Gets the http response based on a PUT request in JSONObject format.
     *
     * @param url the request URL.
     * @param putEntity a PUT request in JSONObject format.
     *
     * @return the string response from PUT request.
     */
    public String getHttpPutResponse(URI url, JSONObject putEntity) {

        String responseAsString = "";
        try {
            HttpPut httpPut = new HttpPut(url);
            StringEntity se = new StringEntity(putEntity.toString());
            se.setContentType(ContentType.APPLICATION_JSON.toString());
            httpPut.setEntity(se);
            HttpResponse response = defaultHttpClient.execute(httpPut);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                responseAsString = EntityUtils.toString(entity, "UTF-8");
                EntityUtils.consume(entity);
            }
        } catch (IOException e) {
            throw new QaRuntimeException("There is a problem with the getPutResponse request. ", e);
        }

        return responseAsString;
    }

    /**
     * Gets the http response based on a delete request in JSONObject format.
     *
     * @param url - the request URL.
     * @return - Httpresponse that will provide the contents of the output file (ID of deleted Template)
     */
    public String getHttpDeleteResponse(URI url) {

        String responseAsString = "";
        try {
            HttpDelete httpDelete = new HttpDelete(url);
            HttpResponse response = defaultHttpClient.execute(httpDelete);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                responseAsString = EntityUtils.toString(entity, CharEncoding.UTF_8);
                EntityUtils.consume(entity);
            }
        } catch (IOException e) {
            throw new QaRuntimeException("There is a problem with the getPostResponse request. ", e);
        }
        return responseAsString;
    }

    /**
     * In this case, we don't care about the entity,
     * but we need to consume it before reuse BasicClientConnectionManager
     *
     * @param url - the request URL.
     * @param postEntity - a post request in JSONObject format.
     */
    public void getHttpPostResponseAndConsumeEntity(URI url, JSONObject postEntity) {
        HttpResponse hr = getHttpPostResponse(url, postEntity);
        logHttpResponseToConsole(hr);
        HttpEntity entity = hr.getEntity();
        if (entity != null) {
            try {
                EntityUtils.consume(entity);
            } catch (IOException e) {
                throw new QaRuntimeException("There is a problem with consume http response. ", e);
            }
        }
    }

    public void logHttpResponseToConsole(HttpResponse hr) {
        try {
            logger.info(EntityUtils.toString(hr.getEntity(), "UTF-8"));
        } catch (Exception e) {
            //Do nothing as we were only trying to output something for debugging.
        }
    }

    /**
     * Gets the http response based on a post request in JSONObject format.
     *
     * @param url - the request URL.
     * @param postEntity - a post request in JSONObject format.
     * @return the httpresponse that will provide the contents of the output file
     * @throws java.io.IOException, JSONException.
     */
    public HttpResponse getHttpPostResponseWithContentTypeText(URI url, JSONObject postEntity) {

        HttpResponse response = null;
        try {
            HttpPost httpPost = new HttpPost(url);
            StringEntity se = new StringEntity(postEntity.toString());
            se.setContentType(ContentType.TEXT_HTML.toString());
            httpPost.setEntity(se);
            response = defaultHttpClient.execute(httpPost);
        } catch (MalformedURLException e) {
            throw new QaRuntimeException("There is a problem with the URL request.  ", e);
        } catch (IOException e) {
            throw new QaRuntimeException("There is a problem with the getPostResponse request. ", e);
        }
        return response;
    }

    /**
     * Perform an HTTP Status check and return the response code
     *
     * @param link - link to check for response status
     * @return - statusCode
     */
    public int getHTTPStatusCode(URI link) {
        HttpClient client = null;
        int result;

        try {
            client = new DefaultHttpClient();
            BasicHttpContext localContext = new BasicHttpContext();
            HttpRequestBase requestMethod = new HttpGet();

            // Set http request
            requestMethod.setURI(link);
            HttpParams httpRequestParameters = requestMethod.getParams();
            httpRequestParameters.setParameter(ClientPNames.HANDLE_REDIRECTS, true);
            requestMethod.setParams(httpRequestParameters);

            // Get response and return return status code
            HttpResponse response = client.execute(requestMethod, localContext);
            result = response.getStatusLine().getStatusCode();
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        } finally {
            HttpClientUtils.closeQuietly(client);
        }

        return result;
    }

    /**
     * Gets the http response based on a post request
     *
     * @param url - requested URL
     * @param nameValuePairs - list of pair parameters used as an element of HTTP messages
     */
    public HttpResponse getHttpValuePairsPostResponse(String url, List<org.apache.http.NameValuePair> nameValuePairs) {

        HttpPost post = new HttpPost(url);
        HttpResponse response;

        // Set up the POST request
        try {
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        } catch (UnsupportedEncodingException e) {
            throw new QaRuntimeException(e);
        }
        try {
            response = defaultHttpClient.execute(post);
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        }

        return response;
    }

    /**
     * Sets HTTP request header
     *
     * @param getMethod - form of an entity identified by the request-URI
     * @param headerName - http header name
     * @param headerValue - http header value
     */
    public void setRequestHeader(GetMethod getMethod, String headerName, String headerValue) {
        getMethod.setRequestHeader(headerName, headerValue);
    }

    private GetMethod getHttpResponseInner (String url, String headerName, String headerValue, NameValuePair[] params)
            {
        org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
        GetMethod getMethod = new GetMethod(url);
        // Set request HTTP header
        setRequestHeader(getMethod, headerName, headerValue);
        // Set HTTP value pair
        if (params != null) {
            getMethod.setQueryString(params);
        }
        try {
        client.executeMethod(getMethod);
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        }
        return getMethod;
    }

    private GetMethod getHttpResponseInner (String url, List<String> headerName, List<String> headerValue, NameValuePair[] params)
    {
        org.apache.commons.httpclient.HttpClient client = new org.apache.commons.httpclient.HttpClient();
        GetMethod getMethod = new GetMethod(url);
        // Set request HTTP header
        for(int i = 0; i <headerName.size();i++) {
            setRequestHeader(getMethod, headerName.get(i), headerValue.get(i));
        }
        // Set HTTP value pair
        if (params != null) {
            getMethod.setQueryString(params);
        }
        try {
            client.executeMethod(getMethod);
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        }
        return getMethod;
    }

    /**
     * Gets the http response as input stream
     *
     * @param url - requested URL
     * @param headerName - http header name
     * @param headerValue - http header value
     * @param params - pair parameters used as an element of HTTP messages.
     */
    public GetMethod getHttpResponse(String url, String headerName, String headerValue, NameValuePair[] params)
           {
        return getHttpResponseInner (url, headerName, headerValue, params);
    }

    /**
     * Gets the http response as input stream
     *
     * @param url - requested URL
     * @param headerName - http header name
     * @param headerValue - http header value
     *
     * @author mxu
     */
    public GetMethod getHttpResponse(String url, String headerName, String headerValue) {
        return getHttpResponseInner (url, headerName, headerValue, null);
    }

    /**
     * Gets the http response as input stream
     *
     * @param url - requested URL
     * @param headerName - http header name
     * @param headerValue - http header value
     *
     * @author mxu
     */
    public GetMethod getHttpResponse(String url, List<String> headerName, List<String> headerValue) {
        return getHttpResponseInner (url, headerName, headerValue, null);
    }

    /**
     * Takes an httpPost object and executes it using defaultHttpClient and returns response as HttpResponse
     *
     * @param httpPost - the post message to send to the server
     * @return an HttpResponse object as returned from the defaultHttpClient
     */
    public HttpResponse getHttpResponse(HttpPost httpPost) {
        try {
            return defaultHttpClient.execute(httpPost);
        } catch (ClientProtocolException e) {
            throw new QaRuntimeException(e);
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        }
    }

    /**
     * Takes an httpPut object and executes it using defaultHttpClient and returns response as HttpResponse
     *
     * @param httpPut the put message to send to the server
     * @return an HttpResponse object as returned from the defaultHttpClient
     */
    public HttpResponse getHttpResponse(HttpPut httpPut) {
        try {
            return defaultHttpClient.execute(httpPut);
        } catch (ClientProtocolException e) {
            throw new QaRuntimeException(e);
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        }
    }

    /**
     * Takes an httpGet object and executes it using defaultHttpClient and returns response as HttpResponse
     * @param httpGet the get message to send to the server
     * @return an HttpResponse object as returned from the defaultHttpClient
     */
    public HttpResponse getHttpResponse(HttpGet httpGet) {
        try {
            return defaultHttpClient.execute(httpGet);
        } catch (ClientProtocolException e) {
            throw new QaRuntimeException(e);
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        }
    }

    public String getHttpResponseAsString(URI url, JSONObject postEntity) {
        HttpResponse hr = getHttpPostResponse(url, postEntity);
        HttpEntity entity = hr.getEntity();
        String response = "";
        if (entity != null) {
            try {
                response = EntityUtils.toString(entity, "UTF-8");
                EntityUtils.consume(entity);
            } catch (IOException e) {
                throw new QaRuntimeException("There was a problem consuming the http response.  ", e);
            }
        }
        return response;
    }

    public JSONObject getResponseAsJson(String url) {
        JSONObject jsonResponse = null;
        String line = "";
        try {
            HttpResponse response = getHttpResponse(new URI(url));

            InputStream ips = response.getEntity().getContent();
            BufferedReader buf = new BufferedReader(new InputStreamReader(ips, Charset.forName("UTF-8")));

            // Message is in first line of response, parse and return it.
            line = buf.readLine();
            jsonResponse = new JSONObject(line);

        } catch (URISyntaxException e) {
            throw new QaRuntimeException(e);
        } catch (IllegalStateException e) {
            throw new QaRuntimeException(e);
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        } catch (JSONException e) {
            throw new QaRuntimeException(e, "Invalid JSON message: '{}'", line);
        }

        return jsonResponse;
    }

    /**
     * Gets an http response using a url
     * @param url the url to hit with the getHttpResponse call
     * @return the JSONArray response from the server or an exception if something went wrong
     */
    public JSONArray getResponseAsJsonArray(String url) {
        JSONArray jsonResponse = null;
        String line = "";
        InputStream inputStream = null;
        BufferedReader bufferReader = null;
        HttpResponse httpResponse = null;
        try {
            httpResponse = getHttpResponse(new URI(url));

            inputStream = httpResponse.getEntity().getContent();
            bufferReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));

            // Message is in first line of response, parse and return it.
            line = bufferReader.readLine();
            jsonResponse = new JSONArray(line);

        } catch (URISyntaxException|IllegalStateException|IOException e) {
            throw new QaRuntimeException(e);
        } catch (JSONException e) {
            throw new QaRuntimeException(e, "Invalid JSON message: '{}'", line);
        } finally {
            EntityUtils.consumeQuietly(httpResponse.getEntity());
            IOUtils.closeQuietly(bufferReader);
            IOUtils.closeQuietly(inputStream);
        }

        return jsonResponse;
    }
}
