package com.automatos.qa.commoncore.sleep;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by dtoms on 3/6/14.
 */
public class QaSleep {

    private static final Logger LOG = LoggerFactory.getLogger(QaSleep.class);

    private QaSleep() {
        throw new QaRuntimeException("Utility class");
    }

    public static void milliseconds(long milliseconds) {

        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException e) {
            LOG.error("Sleep was interrupted.", e);
        }
    }

    public static void seconds(long seconds) {

        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            LOG.error("Sleep was interrupted.", e);
        }
    }

    public static void minutes(long minutes) {

        try {
            TimeUnit.MINUTES.sleep(minutes);
        } catch (InterruptedException e) {
            LOG.error("Sleep was interrupted.", e);
        }
    }

    public static void hours(long hours) {
        // I put this here intentionally to prevent this method being added in the future.
        throw new UnsupportedOperationException("Never sleep for hours!");
    }
}
