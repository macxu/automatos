package com.automatos.qa.commoncore.charset;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

import java.nio.charset.Charset;

public class QaCharSet {
    
    public static final Charset UTF8 = Charset.forName("UTF-8");
    public static final Charset UTF16 = Charset.forName("UTF-16");
    public static final Charset UTF16LE = Charset.forName("UTF-16LE");
    public static final Charset UTF16BE = Charset.forName("UTF-16BE");
    public static final Charset UTF32 = Charset.forName("UTF-32");
    public static final Charset USASCII = Charset.forName("US-ASCII");

    private QaCharSet() {
        throw new QaRuntimeException("Utility class");
    }
}