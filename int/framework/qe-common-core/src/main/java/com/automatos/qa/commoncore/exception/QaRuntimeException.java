package com.automatos.qa.commoncore.exception;

import org.slf4j.helpers.MessageFormatter;

/**
 */
public class QaRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    public QaRuntimeException(Throwable e) {
        super(e);
    }

    public QaRuntimeException(String msg, Throwable e) {
        super(msg, e);
    }

    public QaRuntimeException(String msg) {
        super(msg);
    }

    public QaRuntimeException(String msg, Object... args) {
        super(new MessageFormatter().arrayFormat(msg, args).getMessage());
    }

    public QaRuntimeException(Throwable e, String msg, Object... args) {
        super(new MessageFormatter().arrayFormat(msg, args).getMessage(), e);
    }
}
