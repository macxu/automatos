package com.automatos.qa.commoncore.json;

import com.automatos.qa.commoncore.exception.QaRuntimeException;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.File;
import java.io.IOException;

public class QaJsonObject extends JSONObject{
    
    public QaJsonObject(String JSONObject) throws JSONException {
        super(JSONObject);
    }
    
    public QaJsonObject() {
        super();
    }

    public QaJsonObject(JSONObject jsonObject) throws JSONException {
        super(jsonObject.toString());
    }
    
    public String getStringSafely(String jsonField) {
        try {
            return getString(jsonField);
        } catch (JSONException e) {
            throw new QaRuntimeException("Unable to parse JSON Object for field: {}", jsonField);
        } 
    }
    
    public QaJsonObject getJsonObjectSafely(String jsonField) {
        try {
            return new QaJsonObject(this.getJSONObject(jsonField));
        } catch (JSONException e) {
            throw new QaRuntimeException("Unable to parse JSON Object for field: {}", jsonField);
        }
    }

    public QaJsonObject loadFromXml(String xmlContent) {
        QaJsonObject jsonObject;

        try {
            jsonObject = new QaJsonObject(XML.toJSONObject(xmlContent));
        } catch (JSONException ex) {
            throw new QaRuntimeException("Unable to load xml to json", ex);
        }

        return jsonObject;
    }

    public QaJsonObject loadFromFile(String jsonFilePath) {
        
        QaJsonObject jsonObject;
        try {
            String resourceFile = QaJsonObject.class.getResource(jsonFilePath).getPath();
            String fileContent = FileUtils.readFileToString(new File(resourceFile));
            jsonObject = new QaJsonObject(fileContent);
            
        } catch (IOException ex) {
            throw new QaRuntimeException("Failed to load json File", ex);
        } catch (JSONException ex) {
            throw new QaRuntimeException("Unable to parse JSON file", ex);
        }

        return jsonObject;
    }
}
