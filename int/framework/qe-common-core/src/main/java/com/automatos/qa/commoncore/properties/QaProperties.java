package com.automatos.qa.commoncore.properties;

import java.util.Properties;

/**
 * An extension of the Properties object that first checks System properties.
 * This allows any property to be overridden from the command line.
 */
public class QaProperties extends Properties {

    @Override
    public String getProperty(String key) {

        String value = System.getProperty(key);

        if (value == null) {
            value = super.getProperty(key);
        }

        return value;
    }

    @Override
    public String getProperty(String key, String defaultValue) {

        String value = getProperty(key);

        if (value == null) {
            value = defaultValue;
        }

        return value;
    }
}
