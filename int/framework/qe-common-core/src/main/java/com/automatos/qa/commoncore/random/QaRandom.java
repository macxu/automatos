package com.automatos.qa.commoncore.random;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

import org.apache.commons.lang.RandomStringUtils;

/**
 * This class generates random data for use in testing.
 *  
 * @author mxu
 * 
 */
public class QaRandom {

    /**
     * Generates a random alphanumeric string.
     * 
     * @param prefix
     * @param postfix
     * @param length
     * @return random string
     */
    public String getRandomString(String prefix, String postfix, int length) {

        if (prefix == null)
            throw new QaRuntimeException("Prefix cannot be null.");

        if (postfix == null)
            throw new QaRuntimeException("Postifx cannot be null.");

        return prefix + RandomStringUtils.randomAlphanumeric(length) + postfix;
    }

    /**
     * Generates a random alphanumeric string.
     * 
     * @param prefix
     * @param length
     * @return random string
     */
    public String getRandomString(String prefix, int length) {

        return prefix + RandomStringUtils.randomAlphanumeric(length);
    }

    /**
     * Generates a random alphanumeric string.
     * 
     * @param length
     * @return random string
     */
    public String getRandomString(int length) {

        return RandomStringUtils.randomAlphanumeric(length);
    }

    /**
     * Generates a repeating string in the format 123456789012345...
     * Note that this starts at 1 but does add a 0 after 9 per requirements.
     * 
     * @param length
     * @return sequential string
     */
    public String getSequentialNumericString(int length) {

        StringBuilder sb = new StringBuilder();

        while (sb.length() < length) {
            for (int i = 1; i < 11; i++) {

                if (sb.length() == length)
                    break;
                
                int value = i;

                if (value == 10)
                    value = 0;

                sb.append(value);
            }

        }

        return sb.toString();
    }

    /**
     * Generate a random string whose length is the number of characters specified
     *
     * @param length
     * @return random numeric String with given length
     */
    public String getRandomNumericString(int length) {
        return RandomStringUtils.randomNumeric(length);
    }
}
