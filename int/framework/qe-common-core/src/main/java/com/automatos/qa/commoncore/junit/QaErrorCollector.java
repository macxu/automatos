package com.automatos.qa.commoncore.junit;

import com.automatos.qa.commoncore.string.QaStringUtils;
import com.automatos.qa.commoncore.log.LogRenderer;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hamcrest.Matcher;
import org.junit.rules.ErrorCollector;
import org.junit.runners.model.MultipleFailureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * QA replacement for the JUnit 4 ErrorCollector class.
 * <p/>
 * This class behaves the same as the default ErrorCollector except
 * rather than throwing multiple exception objects, it throws only one.
 * The exception thrown has the aggregated error messages of all errors
 * and the root cause of the exception is set to the first exception in the list.
 * <p/>
 * This is to work around a longstanding unfixed maven surefire/failsafe plugin bug
 * which reports a JUnit failure per contained error, rather than a single failure,
 * which is the correct and desired behavior.
 *
 * @see <a href="http://junit.org/javadoc/4.11/org/junit/rules/ErrorCollector.html">ErrorCollector</a>
 * @see <a href="https://jira.codehaus.org/browse/SUREFIRE-779">SUREFIRE-779</a>
 */
public class QaErrorCollector extends ErrorCollector {
    private static final Logger classLog = LoggerFactory.getLogger(QaErrorCollector.class);

    //Used for logging things in a box so it looks pretty
    private LogRenderer renderer = new LogRenderer();

    /* The superclass doesn't expose its errors collection so we create our own.
     * Methods that access it have all been overridden.
     */
    private List<Throwable> errors = new ArrayList<>();
    private Map<String, String> knownBugs = new HashMap<>();
    private Map<String, String> foundBugs = new HashMap<>();

    /**
     * Simple throwable with no stacktrace.  The constructor string can be used to initialize a
     * throwable with a pre-built stacktrace as the message
     * 
     */
    public class NoStackTraceThrowable extends Throwable {
        public NoStackTraceThrowable(String string) {
            super(string);
        }

        @Override
        public synchronized Throwable fillInStackTrace() {
            return this;
        }
    }

    @Override
    protected void verify() throws Throwable {
        
        //Consolidate the number of errors down to one while checking if any of the errors are known bugs
        consolidateErrorsList();
        
        //We should make sure that all known jira bugs were found and if not, list the unfound bugs so the
        //user knows to go perform a manual check to see if the bug has been fixed
        if (knownBugs.size() > 0) {
            //We still have bugs that we expected.  Since they were not detected they should be listed as bugs
            addKnownBugsToConsolidatedErrorList();
        }
        
        //Throw the consolidated error message
        MultipleFailureException.assertEmpty(errors);
    }
    
    private void consolidateErrorsList() {
        if (errors.size() > 1) {
            
            StringBuilder consolidatedErrorsMessage = new StringBuilder("\n");
            
            for(Throwable throwable : errors) {
                consolidatedErrorsMessage.append(StringUtils.defaultString(getJiraLinkToKnownBug(throwable.getMessage())));
                consolidatedErrorsMessage.append(QaStringUtils.join(ExceptionUtils.getRootCauseStackTrace(throwable), "\n"));
                consolidatedErrorsMessage.append("\n").append("\n");
            }
            
            //Create a no stack trace throwable with the existing stack traces string above.
            //We do this so that the throwable that is created does not have a stack trace pointing
            //to this class, but instead only has the stacktraces from real errors.
            NoStackTraceThrowable consolidatatedErrors = new NoStackTraceThrowable(consolidatedErrorsMessage.toString());

            // create a list to return with only one exception in it.
            List<Throwable> mergedErrors = new ArrayList<>();

            mergedErrors.add(consolidatatedErrors);
            
            errors = mergedErrors;
        }
    }
    
    private void addKnownBugsToConsolidatedErrorList() {
        if (knownBugs.size() > 0) {
            for(String string : knownBugs.keySet()) {
                errors.add(new Throwable("A known bug was not found.  Verify '" + knownBugs.get(string) + "' has been resolved" ));
            }
            
            consolidateErrorsList();
        }
    }

    @Override
    public void addError(Throwable error) {
        List<String> msg = renderer.render(error.getMessage());

        for (String s : msg) {
            classLog.error(s);
        }

        errors.add(error);
    }
    
    public AssertionError fail(String message) {
        return new AssertionError(message);
    }
    
    public void addKnownBug(String expectedErrorMessage, String jiraLink) {
        knownBugs.put(expectedErrorMessage, jiraLink);
    }
    
    /**
     * Retrieves the Jira web url based on the error message
     * @param errorMessage the error message of a known bug
     * @return the JIRA web url or null
     */
    private String getJiraLinkToKnownBug(String errorMessage) {
        String knownBug = knownBugs.get(errorMessage);
        if (StringUtils.isEmpty(knownBug)) {
            
            /**
             * Check the list of bugs already found...if its there, then we previous saw this same bug already,
             *  and we can output the same Existing Bug message.  Useful during sync when the same bug may get
             *  listed twice due to two validations taking place.  Each bug should be listed as a known bug and
             *  will get removed as a part of each validation step.
             */
            knownBug = foundBugs.get(errorMessage);
            if (StringUtils.isEmpty(knownBug)) {
                return null;
            }
        } else {
            foundBugs.put(errorMessage, knownBug);
            knownBugs.remove(errorMessage);
        }
        
        StringBuilder message = new StringBuilder();
        message.append("EXISTING BUG: ");
        message.append(knownBug);
        message.append("\n");
        
        return message.toString();
    }

    public <T> void checkThat(T value, Matcher<T> matcher, String reason, Object... args) {
        super.checkThat(MessageFormatter.arrayFormat(reason, args).getMessage(), value, matcher);
    }

    public List<Throwable> getErrors() {
        return errors;
    }

    public void setErrors(List<Throwable> errorsToSet) {
        errors.clear();

        for(Throwable throwable : errorsToSet) {
            errors.add(throwable);
        }
    }
}
