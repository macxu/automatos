package com.automatos.qa.commoncore.junit.log;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Loads and parses the Javadoc.html corresponding to given fully qualified classname from the classpath. It is assumed
 * the build will put the Javadoc into the classpath. The parsing will only work with JDK 6.
 * <p/>
 * Note the parsing is ugly but it works and the format of javadoc will not change ever unless we move jdks. Javadoc is
 * not xhtml, it is html and as such we'd have to use an HTML parser which is just as painful.
 * 
 * This can be replaced in the future with a custom javadoclet in the build rendering structured data and this class can
 * then load it directly.
 */
public class ClassJavaDoc {

    private Logger log = LoggerFactory.getLogger(ClassJavaDoc.class);

    private Map<String, String> methodDocByName = new HashMap<>();

    private String html;
    private String fullyQualifiedClassName;
    private String className;
    private String classDoc;

    public ClassJavaDoc(String fullyQualifiedClassName) {

        this.fullyQualifiedClassName = fullyQualifiedClassName;
        className = fullyQualifiedClassName.substring(fullyQualifiedClassName.lastIndexOf('.') + 1, fullyQualifiedClassName.length());

        loadAndParse();
    }

    public String getClassJavadoc() {
        return classDoc;
    }

    public String getMethodJavadoc(String name) {
        return this.methodDocByName.get(name);
    }

    private void loadAndParse() {

        loadHtml();

        if (html != null) {

            parseClass();

            parseMethods();
        }
    }

    private void loadHtml() {

        // maven build puts the javadoc here
        String path = "/testapidocs/" + fullyQualifiedClassName.replaceAll("\\.", "/") + ".html";

        InputStream is = getClass().getResourceAsStream(path);

        log.debug("Load Javadoc: " + path);

        if (is != null) {

            try {
                html = IOUtils.toString(is, "UTF-8");
                html = html.replaceAll("&nbsp;", "");

            } catch (IOException e) {
                throw new QaRuntimeException(e);
            }
        }
    }

    /**
     * Parses the class javadoc from the html. In order to understand the parsing, you have to look at the source of a
     * javadoc html file. The format of the javadoc will not change.
     */
    private void parseClass() {
        
        String targetIndexCriteria = "<span class=\"typeNameLabel\">" + className + "</span>";
        int start = html.indexOf(targetIndexCriteria);

        // 6 below is the legnth of the </PRE> tag
        targetIndexCriteria = "<div class=\"block\">";
        start = html.indexOf(targetIndexCriteria, start) + targetIndexCriteria.length();

        int end = html.indexOf("FIELD SUMMARY");
        if (end == -1) {
            end = html.indexOf("<!-- ======== CONSTRUCTOR SUMMARY");
        }

        String chunk = html.substring(start, end).trim();

        chunk = chunk.replace("</B></DT>\n", "");
        chunk = stripHtml(chunk);

        String[] splits = chunk.split("\n");

        StringBuilder output = new StringBuilder();

        /*
         * this flag tracks if we have multiple blank lines in a row. we want to preserve intentional blank lines but
         * not excessive ones caused by stripping html tags.
         */

        boolean lastLineBlank = false;

        for (String line : splits) {

            line = line.trim();

            if (line.length() == 0) {

                if (lastLineBlank) {
                    continue;
                }

                lastLineBlank = true;

            } else {

                lastLineBlank = false;
            }

            if (line.startsWith("<!--")) {
                continue;
            }

            output.append(line + "\n");
        }

        classDoc = output.toString().trim();
    }

    private void parseMethods() {
        
        int start = html.indexOf("<h3>Method Detail</h3>");
        int end = html.indexOf("<!-- ========= END OF CLASS DATA", start);

        String section = html.substring(start, end);
        
        //find end of anchor tag inclusively and set that as the new section
        int startingIndex = section.indexOf('>', section.indexOf("<a name"));
        section = section.substring(startingIndex + 1, section.length()).trim();

        String[] methods = section.split("<!--   -->");

        for (String m : methods) {

            if (m.isEmpty()) {
                continue;
            }
            m = m.trim();

            // 4 below is the length of the <H3> tag
            String name = m.substring(m.indexOf("<h4>") + 4, m.indexOf("</h4>"));
            name = name.replaceAll("\n", "");

            if (m.indexOf("<div class=") > 0) {
                m = m.substring(m.indexOf("<div class="), m.length());
                m = stripHtmlIgnoreBRP(m);
                m = m.trim();
            } else {
                m = "";
            }

            int newlineSpot = m.indexOf("Expected Results:");
            if (newlineSpot > 0) {
                m = m.substring(0, newlineSpot).replaceAll("\n", "") + "\n \n" + m.substring(newlineSpot, m.length()).replaceAll("\n", "");
            }

            m = convertBRPToNewLine(m);

            methodDocByName.put(name, m);
        }
    }

    /**
     * Remove's all tags. Sufficient for logging purposes.
     * 
     * @param line
     * @return
     */
    private String stripHtml(String line) {
        return line.replaceAll("\\<.*?\\>", "");
    }

    /**
     * Remove's all tags. <BR> , <P>, </P> are ignored to preserve formatting of text All extra whitespace is
     * converted to a single space. Eg. multiple tabs, spaces, etc are all converted to a single space.
     * 
     * @param line
     * @return
     */
    private String stripHtmlIgnoreBRP(String line) {
        return line.replaceAll("(?!<br>)(?!<?.p>)\\<.*?\\>", "").replaceAll("\\s+", " ");
    }

    /**
     * Converts <BR> & </P> to '\n'. Converts <P> to '\n\n'. Used to preserve formatting of text
     * 
     * @param line
     * @return
     */
    private String convertBRPToNewLine(String line) {
        String finalStripping = line.replaceAll("(\\<br\\>|\\</p\\>)", "\n").replaceAll("(\\<p\\>)", "\n \n");
        return finalStripping;
    }
}
