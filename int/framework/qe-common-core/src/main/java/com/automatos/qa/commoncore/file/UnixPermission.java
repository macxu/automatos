package com.automatos.qa.commoncore.file;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

import java.util.HashMap;
import java.util.Map;

/**
 * A helper class to convert the permission string rwxrwxrwx to integer format
 *
 */
public class UnixPermission {

    private static Map<String, String> permissionMap = new HashMap<>();
    static{
        permissionMap.put("---", "0");
        permissionMap.put("--x", "1");
        permissionMap.put("-w-", "2");
        permissionMap.put("-wx", "3");
        permissionMap.put("r--", "4");
        permissionMap.put("r-x", "5");
        permissionMap.put("rw-", "6");
        permissionMap.put("rwx", "7");
    }

    private UnixPermission() {
        throw new QaRuntimeException("Utility class");
    }

    /**
     * Convert permission string rwxrwxrwx to decimalnumber
     *
     * @param permissions - the permission string
     * @return decimal number
     */
    public static int getDecimalPermissions(String permissions) {
        return Integer.parseInt(matchPermissionsToNumber(permissions), 8);
    }

    /**
     * convert the permission string rwxrwxrwx to integer format
     *
     * @param permission permission string in Linux system
     * @return the permission in decimal format
     */
    protected static String matchPermissionsToNumber(String permissions) {
        final int expected_length = 10;
        if (permissions == null || permissions.length() != expected_length) {
            throw new QaRuntimeException("Invalid permission string length: expected '10' actual '{}'.", permissions.length());
        } else if (!permissions.substring(1, expected_length).matches("[rwx-]*")) {
            // Make sure last 9 chars are r or x or w or -
            throw new QaRuntimeException("Invalid characters in permission string.  Must contain only r, w, x, or -");
        }
        return permissionMap.get(permissions.substring(1, 4)) + permissionMap.get(permissions.substring(4, 7)) +
        permissionMap.get(permissions.substring(7, 10));
    }
}
