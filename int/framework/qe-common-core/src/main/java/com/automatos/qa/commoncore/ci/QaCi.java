package com.automatos.qa.commoncore.ci;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

public class QaCi {

    private QaCi() {
        throw new QaRuntimeException("Utility class");
    }

    public static boolean isCi() {
        return System.getenv().get("EXECUTOR_NUMBER") !=null;
    }
}
