package com.automatos.qa.commoncore.log;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Renders log messages into a pretty format for logging.
 * This renderer wraps the message into a box surrounded by
 * Unicode Box Drawing characters
 *
 * @see @see <a href="http://unicode-table.com/en/#box-drawing">Box Drawing</a>
 */
public class LogRenderer {

    private static final String TOP_LEFT_CORNER = "\u250C";
    private static final String TOP_RIGHT_CORNER = "\u2510";
    private static final String BOTTOM_LEFT_CORNER = "\u2514";
    private static final String BOTTOM_RIGHT_CORNER = "\u2518";
    private static final String SIDE = "\u2502";
    private static final String DASH = "\u2500";

    // This controls the max width of the box rendered and the lines within it.
    private static final int BOX_WIDTH = 80;

    // used to build the top and bottom of the box
    private static String dashLine;

    // used to add blank lines inside the box
    private static String spaceLine;

    static {
        init();
    }

    /**
     * Set up our static data once for reuse.
     */
    private static void init() {

        StringBuilder sb = new StringBuilder();

        // build our long line once
        for (int i = 0; i < BOX_WIDTH; i++) {
            sb.append(DASH);
        }

        dashLine = sb.toString();

        // create a line of same size but with spaces
        spaceLine = dashLine.replaceAll(DASH, " ");
    }

    /**
     * Render a box with <b>name</b> on a top line followed by <b>description</b>
     *
     * @param name
     * @param description
     * @return
     */
    public List<String> render(String name, String description) {
        String descriptionTemp = description;

        if (description == null) {
            descriptionTemp = "";
        }

        List<String> output = new ArrayList<>();

        output.add(TOP_LEFT_CORNER + dashLine + TOP_RIGHT_CORNER);

        // name line
        output.add(padRight(SIDE + " Name: " + name));

        // separator line
        output.add(SIDE + spaceLine + SIDE);

        output.addAll(blockJustify(descriptionTemp));

        output.add(BOTTOM_LEFT_CORNER + dashLine + BOTTOM_RIGHT_CORNER);

        return output;
    }

    /**
     * Render a box with <b>msg</b>
     *
     * @param msg
     * @return
     */
    public List<String> render(String msg) {

        List<String> output = new ArrayList<>();

        output.add(TOP_LEFT_CORNER + dashLine + TOP_RIGHT_CORNER);

        output.addAll(blockJustify(msg));

        output.add(BOTTOM_LEFT_CORNER + dashLine + BOTTOM_RIGHT_CORNER);

        return output;
    }

    /**
     * Take a description string and block format it.
     *
     * @param description
     * @return
     */
    private ArrayList<String> blockJustify(String description) {

        // reduce excessive line breaks
        String descriptionTemp = description.replaceAll("\n\n", "\n");

        ArrayList<String> lines = new ArrayList<>();

        String[] splits = descriptionTemp.split("\n");

        // -2 saves space for the SIDE characters
        int increment = BOX_WIDTH - 2;

        for (String split : splits) {

            // if the given line is wider than our box, chop and pad it.
            for (int i = 0; i < split.length(); i += increment) {

                int end = i + increment;
                if (end > split.length()) {
                    end = split.length();
                }

                String middle = split.substring(i, end);

                String line = padRight(SIDE + " " + middle.trim());

                lines.add(line);

            }
        }

        return lines;
    }

    /**
     * Creates the right SIDE of the box by padding with spaces then adding the SIDE.
     *
     * @param line
     * @return
     */
    private String padRight(String line) {

        String apachePad = StringUtils.rightPad(line, BOX_WIDTH) + " " + SIDE;

        return apachePad;
    }
}
