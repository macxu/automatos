package com.automatos.qa.commoncore.junit;

import org.junit.internal.AssumptionViolatedException;
import org.junit.rules.TestWatcher;
import org.junit.rules.Verifier;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Copy/Paste of class {@link TestWatcher} refactored so that it surfaces any exceptions thrown during the start of a
 * test via the startingLoudly method.
 *
 * @see http://junit-team.github.io/junit/javadoc/4.11/org/junit/rules/TestWatcher.html
 */
public abstract class QaTestWatcher extends Verifier {
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                QaErrorCollector errors = new QaErrorCollector();
                boolean startedSuccessfully = false;

                try {
                    // If exception is thrown in starting it will surface up and end the suite of tests.
                    startingLoudly(description);
                    startedSuccessfully = true;
                    
                } catch (Throwable t) {
                    // If an error happens during starting, ensure finsihedQuietly method is invoked to handle any cleanup
                    startedSuccessfully = false;
                    errors.addError(t);
                    finishedQuietly(description, errors);
                }
                
                //Only run the remainder of the suite if the starting method succeeded, otherwise skip to outputting the errors that were found
                if (startedSuccessfully) {
                    try {
                        base.evaluate();
                        succeededQuietly(description, errors);
                    } catch (AssumptionViolatedException e) {
                        errors.addError(e);
                        skippedQuietly(e, description, errors);
                    } catch (Throwable t) {
                        errors.addError(t);
                        failedQuietly(t, description, errors);
                    } finally {
                        finishedQuietly(description, errors);
                    }
                }

                //Output all errors that were collected during the TestWatcher execution
                errors.verify();
            }
        };
    }

    private void succeededQuietly(Description description, QaErrorCollector errors) {
        try {
            succeeded(description);
        } catch (Throwable t) {
            errors.addError(t);
        }
    }

    private void failedQuietly(Throwable t, Description description, QaErrorCollector errors) {
        try {
            failed(t, description);
        } catch (Throwable t1) {
            errors.addError(t1);
        }
    }

    private void skippedQuietly(AssumptionViolatedException e, Description description, QaErrorCollector errors) {
        try {
            skipped(e, description);
        } catch (Throwable t) {
            errors.addError(t);
        }
    }

    /**
     * Does not catch any exceptions that occur during starting.  If errors happen during starting, execution is halted.
     */
    private void startingLoudly(Description description) {
        starting(description);
    }

    private void finishedQuietly(Description description, QaErrorCollector errors) {
        try {
            finished(description);
        } catch (Throwable t) {
            errors.addError(t);
        }
    }

    /**
     * Invoked when a test succeeds
     */
    protected void succeeded(Description description) {
    }

    /**
     * Invoked when a test fails
     */
    protected void failed(Throwable e, Description description) {
    }

    /**
     * Invoked when a test is skipped due to a failed assumption.
     */
    protected void skipped(AssumptionViolatedException e, Description description) {
    }

    /**
     * Invoked when a test is about to start
     */
    protected void starting(Description description) {
    }

    /**
     * Invoked when a test method finishes (whether passing or failing)
     */
    protected void finished(Description description) {
    }
}
