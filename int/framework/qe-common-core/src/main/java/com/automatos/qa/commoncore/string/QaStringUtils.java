package com.automatos.qa.commoncore.string;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.automatos.qa.commoncore.json.QaJsonObject;
import com.automatos.qa.commoncore.exception.QaRuntimeException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class QaStringUtils extends StringUtils {
    
    /**
     * Simple method for extracting a String value from a JSON that is currently stored as a string.
     * @param serverResponse the server response in json as a string
     * @param jsonField the json field to extract
     * @return a string representation of the json field extracted from the json string
     */
    public static String getStringValueFromJSON(String serverResponse, String jsonField) {
        //Check if response has value
          if (!StringUtils.isEmpty(serverResponse)) {
              try {
                  //parse the value out
                  return new QaJsonObject(serverResponse).getStringSafely(jsonField);
              } catch (JSONException e) {
                  throw new QaRuntimeException("Unable to parse JSON Object for field: {}.  Server Response: {}", jsonField, serverResponse);
              }
          }
          //Return an empty string so if someone performs .toString() or other String related method they don't run into a NPE.
          return "";
    }

    /**
     * Converts JSON string to Map
     *
     * @param jsonString- JSON String
     * @return Object of type Map<String,Object>
     */
    public static Map<String,Object> getMapObjectFromJSONString(String jsonString){
        if(QaStringUtils.isEmpty(jsonString)){
            throw new QaRuntimeException("jsonString cannot be null");
        }
        try{
            ObjectMapper mapper = new ObjectMapper();
            return (Map<String, Object>) mapper.readValue(jsonString, HashMap.class);
        }catch(IOException e){
            throw new QaRuntimeException("Error converting JSON string to Map", e);
        }
    }

    /**
     * Easy way to convert a list of any object like (Long, Integer, Double etc.) to String list
     *
     * @param from the source list
     * @param conversionFunction function that will do the conversion
     * @param <T> Generic Data Type
     * @return List<String>
     */
    public static <T> List<String> convertToStringList(List<T> from, Function<T, String> conversionFunction) {
        return from.stream().map(conversionFunction).collect(Collectors.toList());
    }

    /**
     * Easy way to convert a list of any object like (Long, Integer, Double etc.) to a String value
     *
     * @param givenList list of elements of type Double, Integer, String etc.
     * @param <T>
     * @return String containing list elements separated by comma
     */
    public static <T> String convertStringListToString(List<T> givenList) {
        return (CollectionUtils.isEmpty(givenList)) ? "" : givenList.stream().map(Object::toString).collect(Collectors.joining(", "));
    }

    /**
     * Utility method to test that two string values are same when the whitespaces are not relevant
     * @param compareWith eg. {"18", "65"}
     * @param compareTo eg. {"18","65"}
     * @return true/ false
     */
    public static boolean isEqualsIgnoreWhiteSpace(String compareWith, String compareTo) {
        if (isEmpty(compareWith) && isEmpty(compareTo)) {
            return true;
        } else if (isEmpty(compareWith) && isNotEmpty(compareTo)) {
            return false;
        } else if (isNotEmpty(compareWith) && isEmpty(compareTo)) {
            return false;
        } else {
            String whitespacePattern = "\\s+";
            return compareWith.replaceAll(whitespacePattern,"").equals(compareTo.replaceAll(whitespacePattern, ""));
        }
    }

    public static boolean isLong(String valueString) {
        try {
            Long.parseLong(valueString);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isFloat(String valueString) {
        try {
            Float.parseFloat(valueString);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isInteger(String valueString) {
        try {
            Integer.parseInt(valueString);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isBoolean(String valueString) {
        try {
            Boolean.parseBoolean(valueString);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public static boolean isDouble(String valueString) {
        try {
            Double.parseDouble(valueString);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
