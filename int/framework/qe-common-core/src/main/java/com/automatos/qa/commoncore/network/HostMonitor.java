package com.automatos.qa.commoncore.network;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.nio.channels.SocketChannel;
import java.util.concurrent.TimeUnit;

/**
 * Launches a thread that attempts to open a socket to the provided host every second.
 * If it fails to connect, it logs a message. This is intended to help debug test issues
 * where the network or host becomes unavailable.
 */
public class HostMonitor {

    private static final Logger log = LoggerFactory.getLogger(HostMonitor.class);

    private Thread monitorThread;

    private URL hostURL;
    private boolean run;
    private boolean stopped;
    private boolean loggedFirstConnection;
    private boolean loggedUnresolvedHostname;

    private int consecutiveFailureSeconds;

    // in seconds
    private int stopFloodCount = 20;
    private int floodPolicy = 10;
    private int sleepInterval;

    private int maxTimeout;

    private Runnable monitor = new Runnable() {

        private boolean connected = false;
        private SocketChannel channel = null;

        @Override
        public void run() {

            String host = hostURL.getHost();
            int port = assignPort(hostURL);

            // loop forever until someone calls the stop method
            while (run) {

                InetSocketAddress socketAddress = new InetSocketAddress(host, port);

                if (socketAddress.isUnresolved()) {

                    logUnresolved(host);
                } else {

                    checkConnection(socketAddress);
                }

                // check connection may have canceled the run
                if (run) {

                    try {
                        TimeUnit.SECONDS.sleep(sleepInterval);
                    } catch (InterruptedException e) {
                        // don't report this
                    }

                    // check again so we don't log incorrectly
                    checkConnection(socketAddress);

                    if (!connected && run) {

                        logConnectionFailure(host);
                    }

                    if ((maxTimeout != -1) && (consecutiveFailureSeconds >= maxTimeout)) {
                        run = false;
                        log.error("Maximum timeout of {} seconds reached, stopping.", maxTimeout);
                    }
                }
            }

            log.info("Host Monitor has stopped.");

            stopped = true;
        }

        private void checkConnection(InetSocketAddress socketAddress) {

            try {

                if (channel == null) {
                    // try to open a connection
                    channel = SocketChannel.open();
                    channel.configureBlocking(false);
                    channel.connect(socketAddress);
                }

                channel.finishConnect();

                if (channel.isConnected()) {
                    handleConnection();
                } else {
                    connected = false;
                }

            } catch (IOException e) {
                connected = false;
            }
        }

        private void handleConnection() {
            connected = true;

            if (consecutiveFailureSeconds > 0 && loggedFirstConnection) {

                log.info("Contact reestablished.");
                sleepInterval = 1;
                loggedUnresolvedHostname = false;

            } else if (!loggedFirstConnection) {

                // if the host is reachable the first time through, log it, only once.
                log.info("Host is reachable.");
                loggedFirstConnection = true;
            }

            // if we've reached here we know we can reach the host
            consecutiveFailureSeconds = 0;

            IOUtils.closeQuietly(channel);
            channel = null;

            // if we are running with a timeout and we can connect, stop now.
            if (maxTimeout != -1) {
                run = false;
            }
        }

        private void logUnresolved(String host) {

            // dns is down or unreachable

            if (!loggedUnresolvedHostname) {
                log.error("Unable to resolve hostname {}.", host);
                loggedUnresolvedHostname = true;
            }

            connected = false;
        }

        private int assignPort(URL url) {
            int port = url.getPort();

            if (port == -1) {
                port = 80;
            }
            return port;
        }

        private void logConnectionFailure(String host) {

            // we were unable to connect to the host

            consecutiveFailureSeconds += sleepInterval;

            log.error("Unable to contact host '{}' for {} seconds", host, consecutiveFailureSeconds);

            // flood protection if the host is down for a long time, stop logging every second.
            if (consecutiveFailureSeconds >= stopFloodCount && sleepInterval == 1) {

                log.info("Unreachable for more than {} seconds.  Changing sleep policy to every {} seconds.", stopFloodCount, floodPolicy);
                sleepInterval = floodPolicy;

            }
        }
    };

    public HostMonitor(URL url) {
        this.hostURL = url;
    }

    /**
     * This method starts the monitor asynchronously.
     */
    public void start() {

        log.info("Host Monitor is monitoring '{}'", hostURL.toExternalForm());

        // -1 means there is no timeout, run until stopped
        maxTimeout = -1;
        startThread();
    }

    /**
     * This method will block until the host is reached or the timeout occurs.
     * This is intended to be used by tests in a BeforeClass or Preflight to bail out immediately if the host is down.
     *
     * @param timeoutInSeconds
     * @return
     */
    public boolean startAndBlockUntilReachable(int timeoutInSeconds) {

        log.info("Host Monitor is waiting for: {}", hostURL.toExternalForm());
        log.info("Timeout is set to {} seconds.", timeoutInSeconds);

        maxTimeout = timeoutInSeconds;
        Thread t = startThread();

        try {
            t.join();
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }

        if (consecutiveFailureSeconds != 0) {
            return false;
        }

        return true;
    }


    public int getConsecutiveFailureSeconds() {
        return consecutiveFailureSeconds;
    }

    private Thread startThread() {

        run = true;
        stopped = false;
        consecutiveFailureSeconds = 0;
        loggedFirstConnection = false;
        sleepInterval = 1;
        loggedUnresolvedHostname = false;

        monitorThread = new Thread(monitor);
        monitorThread.start();
        return monitorThread;
    }

    public void stop() {

        run = false;
        monitorThread.interrupt();
    }

}
