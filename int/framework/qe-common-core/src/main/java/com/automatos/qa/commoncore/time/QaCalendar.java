package com.automatos.qa.commoncore.time;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Class for time related methods.
 *
 */
public class QaCalendar {

    private static final Logger log = LoggerFactory.getLogger(QaCalendar.class);

    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static final String DATE_PATTERN = "yyyy-MM-dd";

    /**
     * Gets today's date.
     *
     * @param String dtFormat represents date format e.g. "MMddyyyy".
     */
    public String getNowAsString(String dtFormat) {
        DateTimeFormatter format = DateTimeFormat.forPattern(dtFormat);
        DateTime date = new DateTime();
        return date.toString(format);
    }

    /**
     * Adjust date by specified number from now to the past.
     *
     * @param String dtFormat represents date format e.g. "MMddyyyy".
     * @param Integer numberOfDays represents number of days to adjust to the past.
     */
    public String getNowMinusDays(String dtFormat, int numberOfDays) {
        if (numberOfDays < 0) {
            throw new QaRuntimeException("Argument 'numberOfDays' less than 0");
        }
        DateTimeFormatter format = DateTimeFormat.forPattern(dtFormat);
        DateTime date = new DateTime();
        return date.minusDays(numberOfDays).toString(format);
    }

    /**
     * Adjust date by specified number from now to the past, converts and returns date to match destination locale. <p>
     *
     * @param Locale destinationLocale represents the locale to which the date needs to be converted. For example, dd/MM/yy for UK, MM/dd/yy for US.
     * @param int numberOfDays represents number of days to adjust to the past.
     *
     * @return locale specific date after adjusting by specified days to the past.
     */
    public String getNowMinusDays(Locale destinationLocale, int numberOfDays){
        if (numberOfDays < 0) {
            throw new QaRuntimeException("Argument 'numberOfDays' less than 0");
        }

        DateTime date = new DateTime();
        DateTime nowMinusDays = date.minusDays(numberOfDays);

        //gets the date format for the locale. For example, dd/MM/yy for UK, MM/dd/yy for US.
        String pattern = DateTimeFormat.patternForStyle("S-", destinationLocale);
        return nowMinusDays.toString(pattern);
    }

    /**
     * Adjust date by specified number from now to the future.
     *
     * @param String dtFormat represents date format e.g. "MMddyyyy".
     * @param Integer numberOfDays represents number of days to adjust to the future.
     */
    public String getNowPlusDays(String dtFormat, int numberOfDays) {
        DateTimeFormatter format = DateTimeFormat.forPattern(dtFormat);
        DateTime date = new DateTime();
        return date.plusDays(numberOfDays).toString(format);
    }

    /**
     * Gets first day of the month.
     *
     * @return first day of the month.
     */
    public Date getFirstDayOfMonth() {
    	Date todayDate = new Date();
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(todayDate);
        calendar.set(Calendar.DAY_OF_MONTH, 1);  
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date firstDateOfMonth = calendar.getTime();
    	log.debug("First date of the month: {}.", firstDateOfMonth);

        return firstDateOfMonth;
    }

    /**
     * Gets last day of the month.
     *
     * @return last day of the month.
     */
    public Date getLastDayOfMonth() {
    	Date todayDate = new Date();
    	Calendar calendar = Calendar.getInstance();  
    	calendar.setTime(todayDate);  
    	calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));  
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    	Date lastDateOfMonth = calendar.getTime();
    	log.debug("Last date of the month: {}.", lastDateOfMonth);
    	
    	return lastDateOfMonth;
    }
    
    /**
     * Takes the number of milliseconds as a long and converts it to seconds and returns the value as a String.
     *
     * @param numberOfMilliseconds the number of milliseconds as a long.
     * @return the number of seconds represented as a String.
     */
    public String toSeconds(long numberOfMilliseconds) {
        return String.valueOf(TimeUnit.MILLISECONDS.toSeconds(numberOfMilliseconds));
    }

    /**
     * Return the current date time with only value for hours (mins and seconds will be 00).
     *
     * @return current data time for GMT timezone without mins and seconds.
     */
    public Date getCurrentDateTimeWithOnlyHours() {
        Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Return a date by adding given number of hours to given date.
     *
     * @param date to add given hour.
     * @param hr - number hours to add.
     * @return new date by adding given hours.
     */
    public Date getDatePlusHours(Date date, int hr) {

    	Calendar currentTime = new GregorianCalendar();
    	currentTime.setTimeInMillis(date.getTime());
    	int currentHr = currentTime.get(Calendar.HOUR);
    	currentTime.add(Calendar.HOUR, currentHr + hr);
    	return currentTime.getTime();
    }

    /**
     * Gets today date without hours, minutes, seconds and milliseconds
     *
     * @return today date up to day of month.
     */
    public Date getCurrentDay() {
        Date todayDate = new Date();
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(todayDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date currentDate = calendar.getTime();
        log.debug("Current Day: {}.", currentDate);

        return currentDate;
    }

}
