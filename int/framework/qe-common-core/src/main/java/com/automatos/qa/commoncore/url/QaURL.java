package com.automatos.qa.commoncore.url;

import com.automatos.qa.commoncore.exception.QaRuntimeException;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

/**
 * Helper methods for constructing the proper url
 *
 */
public class QaURL {
    private static final String QUERY_START = "?";

    private URL url;

    public QaURL(String url)  {
        try{
            this.url = new URL(url);
        }catch (MalformedURLException e) {
            throw new QaRuntimeException("Unable to create valid URL. ", e);
        }
    }

    public URL getUrl() {
        return this.url;
    }

    public URI getUri() {
        try {
            return url.toURI();
        } catch (URISyntaxException e) {
            throw new QaRuntimeException("Could not construct valid URI on URL.", e);
        }
    }

    /**
     * Append url params into the url for HTTP requests.
     *
     * Example:
     * urlParams: "isForceSync", "true", "pcaId", "101"
     * appended url: http://localhost:5678/public?isForceSync=true&pcaId=101
     *
     * @param urlParams param list of <paramKey, paramValue>
     */
    public void appendParams(String... urlParams) {
        StringBuilder urlBuilder = new StringBuilder(this.getUrl().toString());
        QaURLParams params = new QaURLParams(urlParams);

        urlBuilder.append(QUERY_START);
        urlBuilder.append(params.getUrlParamString());
        try {
            this.url = new URL(urlBuilder.toString());
        } catch (MalformedURLException e) {
            throw new QaRuntimeException("Invalid url params provided. Unable to add", e);
        }

    }

    /**
     * Append url params into the url for HTTP requests.
     *
     * @param urlParams param Map of <paramKey, paramValue>
     */
    public void appendParams(Map<String, Object> urlParams) {

        StringBuilder urlBuilder = new StringBuilder(this.getUrl().toString());
        QaURLParams params = new QaURLParams(urlParams);

        urlBuilder.append(QUERY_START);
        urlBuilder.append(params.getUrlParamString());

        try {
            this.url = new URL(urlBuilder.toString());
        } catch (MalformedURLException e) {
            throw new QaRuntimeException("Invalid url params provided. Unable to add", e);
        }
    }
}
