package com.automatos.qa.commoncore.file.read;

import com.automatos.qa.commoncore.charset.QaCharSet;
import com.automatos.qa.commoncore.ci.QaCi;
import com.automatos.qa.commoncore.exception.QaRuntimeException;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;

public class QaIOUtils extends IOUtils {

    public static final String UTF16LE_BOM = Character.toString((char) 65279);

    private static final int BOM_LEN = 3;

    private static final byte[]
            UTF_16_BOM = { (byte) 0xFF, (byte) 0xFE },
            UTF_16_BE_BOM = { (byte) 0xFE, (byte) 0xFF },
            UTF_8_BOM = { (byte) 0xEF, (byte) 0xBB, (byte) 0xBF },
            EOF = { (byte) 0x04 };

    /**
     * Opens a file and returns it as an OutputStream.  Throws a QaRuntimeException if file is not found
     * @param filepath the file object representing the file to be opened as an OutputStream
     * @return the OutputStream of the file
     */
    public static OutputStream openOutputStream(File filepath) {
        OutputStream stream;
        try {
            stream = new FileOutputStream(filepath);
        } catch (FileNotFoundException e) {
            throw new QaRuntimeException("Unable to load file: {}", filepath.getPath());
        }
        return stream;
    }

    /**
     * Opens a file and returns it as an InputStream.  Throws a QaRuntimeException if file is not found
     * @param filepath the file object representing the file to be opened as an InputStream
     * @return the InputStream of the file
     */
    public static InputStream openInputStream(File filepath) {
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(filepath);
        } catch (FileNotFoundException e) {
            throw new QaRuntimeException("Unable to load file: {}", filepath.getPath());
        }
        return inputStream;
    }

    /**
     * Opens a file and returns it as an InputStream.  Throws a QaRuntimeException if file is not found
     * @param filePath the path the file to be opened as an InputStream
     * @return the InputStream of the file
     */
    public static InputStream openInputStream(String filePath) {
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(new File(filePath));
        } catch (FileNotFoundException e) {
            throw new QaRuntimeException("Unable to load file: {}", filePath);
        }
        return inputStream;
    }

    public static File getLocalFile(String resourcePath) {

        File file;

        URL url = QaIOUtils.class.getResource(resourcePath);

        if(url == null) {
            throw new QaRuntimeException("Classpath resource at '{}' does not exist.", resourcePath);
        }

        file = new File(QaIOUtils.class.getResource(resourcePath).getFile());

        if(!file.exists()) {
            if (QaCi.isCi()) {
                //Check to see if we have bad characters in the path due to running concurrent builds on jenkins
                String path = file.getAbsolutePath();
                path = path.replace("%40", "@");
                File ciFile = new File(path);
                if (ciFile.exists()) {
                    return ciFile;
                } else {
                    throw new QaRuntimeException("Classpath resource at '{}' (Full Path: '{}') does not resolve to a local file.", resourcePath, ciFile.getAbsolutePath());
                }
            }

            throw new QaRuntimeException("Classpath resource at '{}' (Full Path: '{}') does not resolve to a local file.", resourcePath, file.getAbsolutePath());
        }

        return file;
    }

    /**
     * Opens a resource specified by path and returns it as a string.
     *
     * @param resourcePath the location of the resource to be opened
     * @return the string representation of the file
     */
    public static String toString(String resourcePath) {
        String inputInString = null;

        try {
            InputStream inputStream = QaIOUtils.class.getResourceAsStream(resourcePath);
            inputInString = IOUtils.toString(inputStream);
        } catch (IOException e) {
            throw new QaRuntimeException("Classpath resource at '{}' does not resolve to a local file.", resourcePath);
        }

        return inputInString;
    }
    
    public static BufferedReader getBufferedReaderFromInputStream(InputStream inputStream) {
        InputStreamReader iReader = new InputStreamReader(inputStream);
        BufferedReader bReader = new BufferedReader(iReader);
        return bReader;
    }
    
    /**
     * Reads in a file and performs required replacements.
     */
    public static File readFileAndReplace(File inputFile, File outputFile, Map<String, String> replacementsToMake) {
        String inputString;

        //Read in the input file
        try (InputStream iStream = new FileInputStream(inputFile)){
            inputString = IOUtils.toString(iStream);
        } catch (IOException e) {
            throw new QaRuntimeException("Unable to read file at: {}", inputFile);
        }
        
        //Replace the required strings
        for (String replacement : replacementsToMake.keySet()) {
            inputString = inputString.replaceAll(replacement, replacementsToMake.get(replacement));
        }
        
        //Write the file
        try (OutputStream oStream = new FileOutputStream(outputFile)){
            IOUtils.write(inputString, oStream);
        } catch (IOException e) {
            throw new QaRuntimeException("Unable to write to file: {}", outputFile);
        }
        
        return outputFile;
    }

    /**
     * Opens a InputStream and returns it as a string.
     *
     * @param inputStream the inputStream to be read
     * @return the string representation of the inputStream
     */
    public static String toString(InputStream inputStream) {
        try {
            return IOUtils.toString(inputStream);
        } catch (IOException e) {
            throw new QaRuntimeException(e);
        }
    }

    /**
     * Get the encoding of the file
     *
     * @param file The input file
     * @return Character encodeing or null if encoding not found
     */
    public static Charset getEncoding(File file) {


        try (FileInputStream  fileInputStream = new FileInputStream(file)){
            // check encoding type which may be contained in the first 2 bytes
            byte[] bom = new byte[BOM_LEN];

            fileInputStream.read(bom);

            if (bom[0] == UTF_8_BOM[0] && bom[1] == UTF_8_BOM[1] && bom[2] == UTF_8_BOM[2]) {
                return QaCharSet.UTF8;
            } else if (bom[0] == UTF_16_BOM[0] && bom[1] == UTF_16_BOM[1]) {
                return QaCharSet.UTF16;
            } else if (bom[0] == UTF_16_BE_BOM[0] && bom[1] == UTF_16_BE_BOM[1]) {
                return QaCharSet.UTF16BE;
            }
        } catch (IOException e) {
            throw new QaRuntimeException("Failed to open input stream", e);
        }

        return null;
    }
}
