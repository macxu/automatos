package com.automatos.qa.demo;

import com.automatos.qa.common.integration.fojos.TestResource;
import com.automatos.qa.commoncore.junit.QaErrorCollector;
import com.automatos.qa.commoncore.junit.log.TestLogger;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.hamcrest.Matchers;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@RunWith(JUnitParamsRunner.class)
public class StringProcessorServiceTest {

    private static final Logger classLog = LoggerFactory.getLogger(StringProcessorServiceTest.class);

    @ClassRule
    public static final TestResource resources = new TestResource();

    @Rule
    public QaErrorCollector assertions = new QaErrorCollector();

    @Rule
    public TestLogger log = new TestLogger(classLog);

    @Test
    @FileParameters(value = "classpath:com/automatos/qa/demo/stringReserveAndMergeTests.json", mapper = TestDefinitionMapper.class)
    public void stringProcessorTest(TestDefinition testDefinition) {

        testDefinition.describe(log);

        log.info("original string: " + testDefinition.getOriginalString());
        log.info("expected string: " + testDefinition.getExpectedResultString());

        String actualResultString = resources.stringService().reverseAndMerge(testDefinition.getOriginalString());

        assertions.checkThat("does not match", actualResultString, Matchers.equalTo(testDefinition.getExpectedResultString()));

    }

}