package com.automatos.qa.demo;

import org.junit.Assert;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Created by mxu on 10/22/17.
 */
public class TryAssertTest {

    @Test
    public void tryAssertEquals() {

        Integer ATwinAge = 10;
        Integer AnotherTwinAge = 10;

        assertEquals("The ages of twins should equal", ATwinAge, AnotherTwinAge);
    }

    @Test
    public void tryAssertNotEquals() {

        Long userIdA = 10010L;
        Long userIdB = 10086L;

        Assert.assertNotEquals("The user IDs should be identical", userIdA, userIdB);
    }

    @Test
    public void tryAssertNotNull() {

        String dream = "I have a dream";

        Assert.assertNotNull("A dream should NEVER be null", dream);
    }

    @Test
    public void tryAssertNull() {

        // query the user from DB with an invalid ID
        Object user = null;

        Assert.assertNull("Invalid user ID should NOT exist", user);
    }

    @Test
    public void tryAssertTrue() {

        Boolean isValidUser = true;

        Assert.assertTrue("This should be a valid user", isValidUser);
    }

    @Test
    public void tryAssertNotSame() {

        Object show1 = new String("Breaking bad");
        Object show2 = new String("Breaking bad");

        // Asserts that two objects refer to different objects
        Assert.assertNotSame("Should not be identical", show1, show2);
    }

    @Test
    public void tryAssertThat() {

        String show1 = "Matrix";
        String show2 = "Matrix reloaded";

        // Asserts that two objects refer to different objects
        Assert.assertThat("Full name should start with short name", show2, startsWith(show1));
    }
}
