package com.automatos.qa.demo;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mxu on 10/21/17.
 */
public class StringProcessorUnitTest {

    @Test
    public void reverseAndMerge() {

        String input = "aaabbccca";
        String expected = "acba";

        String actual = StringProcessor.reverseAndMerge(input);

        Assert.assertEquals("reverse and merge failed", expected, actual);
    }
}
