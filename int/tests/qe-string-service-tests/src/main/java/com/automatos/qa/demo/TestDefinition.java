package com.automatos.qa.demo;

import com.automatos.qa.common.integration.testdefinition.BaseTestDefinition;

/**
 * Test definition for string service testing
 *
 */
public class TestDefinition extends BaseTestDefinition {

    private String originalString;
    private String expectedResultString;

    public String getOriginalString() {
        return originalString;
    }

    public void setOriginalString(String originalString) {
        this.originalString = originalString;
    }

    public String getExpectedResultString() {
        return expectedResultString;
    }

    public void setExpectedResultString(String expectedResultString) {
        this.expectedResultString = expectedResultString;
    }
}
