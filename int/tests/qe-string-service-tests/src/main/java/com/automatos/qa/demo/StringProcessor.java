package com.automatos.qa.demo;

/**
 * Created by mxu on 10/21/17.
 */
public class StringProcessor {

    public static String reverseAndMerge(String inputString) {

        if (inputString == null || inputString.length() < 2) {
            return inputString;
        }

        StringBuilder resultString = new StringBuilder();
        resultString.append(inputString.charAt(inputString.length() - 1));

        for (int index = inputString.length() - 2; index > 0; index--) {

            if (inputString.charAt(index) == resultString.charAt(resultString.length() - 1)) {
                continue;
            }

            resultString.append(inputString.charAt(index));
        }

        return resultString.toString();
    }
}
