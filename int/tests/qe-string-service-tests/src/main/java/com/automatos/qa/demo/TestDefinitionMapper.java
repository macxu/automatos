package com.automatos.qa.demo;

import com.fasterxml.jackson.core.type.TypeReference;
import com.automatos.qa.common.integration.testdefinition.BaseTestDefinitionMapper;
import com.automatos.qa.commoncore.junit.mapper.JsonMapper;

import java.util.List;

/**
 * Class that explains how the {@link JsonMapper} is supposed to
 * match on the list of {@link TestDefinition} contained
 * in the input files of a test.
 *
 */

public class TestDefinitionMapper extends BaseTestDefinitionMapper {

    @Override
    public TypeReference getValueTypeReference() {
        return new TypeReference<List<TestDefinition>>() {
        };
    }

}
